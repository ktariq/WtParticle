from __future__ import print_function
import re
import sys

try:
    from pathlib2 import PosixPath
except ImportError:
    raise ImportError('Could not import pathlib2')

from .utils import get_process
from .utils import get_simtype
from .utils import get_campaign

import subprocess
import os

import logging
logger = logging.getLogger(__name__)


class Job(object):
    """Describes a WtLoop job"""

    def __init__(self, rucio_ds_path, config_path, tree='particleLevel',
                 output_root='.', enable_plevel=False, saver='SaverNone'):
        self.rdp = PosixPath(rucio_ds_path)
        self.config = PosixPath(config_path)
        self.tree = tree
        if not self.rdp.exists():
            raise ValueError('{} path does not exist'.format(rucio_ds_path))
        if not self.config.exists():
            raise ValueError('{} path does not exist'.format(config_path))
        self.rdp = self.rdp.absolute().resolve()
        self.config = self.config.absolute().resolve()
        self.campaign = get_campaign(self.rdp.name)
        self.saver = saver
        self.simtype = get_simtype(self.rdp.name)
        self.is_data = self.campaign == 'Data'
        self.enable_pl = enable_plevel
        if self.is_data:
            self.dsid = re.search('(data[0-9]{2})', self.rdp.name).group(0)
        else:
            self.dsid = re.search('([0-9]{6})', self.rdp.name).group(0)
        self.process = get_process(self.dsid)
        self.file_list = list(self.rdp.glob('*.root*'))

        self.output_dir = PosixPath(output_root).resolve()
        self.output_base = self.output_dir / self.name
        self.output = self.output_base.with_suffix('.root')
        self.stderr = self.output_base.with_suffix('.stderr.log')
        self.stdout = self.output_base.with_suffix('.stdout.log')
        self.script_file = self.output_base.with_suffix('.sh')

    def __repr__(self):
        """for printing job information"""
        return 'Job(dataset = {},\n    config = {},\n    tree = {}\n)'.format(
            self.rdp, self.config, self.tree)

    @property
    def name(self):
        return '{process}_{dsid}_{simtype}_{campaign}_{tree}'.format(
            **self.__dict__)

    @property
    def command(self):
        com = '{exe} {dp} {conf} -d {rdir} -t {tree} -o {out} {pl} --saver {s}'.format(
            exe='runWtParticle',
            conf=self.config,
            rdir=self.rdp,
            tree=self.tree,
            out=self.output,
            pl='--enable-plevel' if self.enable_pl else '',
            dp='--disable-progress',
            s=self.saver
        )
        return com

    @property
    def script(self):
        return '\n'.join(l[8:] for l in u"""
        #!/bin/bash

        echo "===> Information"
        echo "Hostname is: $(hostname)"
        initdir="$PWD"
        echo "Started in: $initdir"
        tmpdir="$(mktemp -d "tmp.wt-loop.XXXXXXX")"
        cd "$tmpdir"
        echo "Changed into: $tmpdir"

        echo "===> Setting up software"
        export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
        source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
        lsetup "asetup {analysis_top}"
        . "{cmake_setup}"

        echo "===> Running job"
        {{(
            set -e
            {command}
        )}} >"{stdout}" 2>"{stderr}"
        status=$?
        echo "Finished with status $status"
        echo "Cleaning up temporary directory $tmpdir"
        cd "$initdir"
        rm -rf "$tmpdir"
        exit $status
        """.split('\n')[1:]).format(
            name = self.name,
            stdout = self.stdout,
            stderr = self.stderr,
            analysis_top = self.get_asetup(),
            cmake_setup = self.get_cmake(),
            command = self.command)

    def get_cmake(self):
        prefix = os.getenv('CMAKE_PREFIX_PATH')
        local = prefix.split(':')[0]
        return os.path.join(local, 'setup.sh')

    def get_asetup(self):
        # TODO check if this works on a release rather than nightly
        # There's also AnalysisTop_VERSION
        branch = os.getenv('AtlasBuildBranch')
        stamp = os.getenv('AtlasBuildStamp')
        platform = os.getenv('AnalysisTop_PLATFORM')
        return '--platform={} AnalysisTop,{},{}'.format(platform, branch, stamp)

    def print_summary(self):
        logger.info('runWtParticle {} -d {} -t {} --saver {} -o {}{}{}'.format(
            self.config.name, self.rdp, self.tree, self.saver, self.output,
            ' --enable-plevel' if self.enable_pl else '',
            ' --disable-progress'))


class Runner(object):
    """Abstract runner class, requires a submit function"""

    def submit_all(self, jobs):
        logger.info('Submitting {} jobs {}'.format(len(jobs), self.describe()))

        for job in jobs:
            self.submit(job)


class DryRunner(Runner):
    def __init__(self):
        pass

    def submit(self, job):
        job.print_summary()

    def describe(self):
        return 'Dry'


class PBSRunner(Runner):
    ''' Uses the PBS batch system to execute jobs. '''

    sites = [ 'sydney', 'boston' ]

    def __init__(self, queue, site = None):
        self.queue = queue

        # The site parameter allows tuning for local PBS quirks:
        if site not in PBSRunner.sites and site is not None:
            raise RuntimeError('Unknown PBSRunner site: {}'.format(site))
        self.site = site

    def describe(self):
        return 'with PBS (queue = {})'.format(self.queue)

    def submit(self, job, attempts = 3):
        sublog = job.output_base.with_suffix('.qsub.log')
        if sublog.exists():
            logger.info('Skipping job that seems to exist already: {}'.format(job.name))
            return
        logger.info('Submitting job: {} to queue {}'.format(job.name, self.queue))

        name = job.name
        if self.site in [ 'boston' ]:
            name = 'x' + name

        joblog = job.output_base.with_suffix('.job.log')
        qsub = ['qsub',
                '-q', self.queue,
                '-N', name,
                '-o', str(joblog),
                '-v', 'WTLOOP_BATCH=1',
                ]
        if self.site in [ 'boston' ]:
            qsub.extend(['-e', joblog])
        else:
            qsub.extend(['-j', 'oe'])

        job.script_file.parent.mkdir(exist_ok = True)
        job.script_file.write_text(job.script)
        for attempt in range(attempts):
            try:
                with job.script_file.open() as script_io:
                    output = subprocess.check_output(qsub, stdin=script_io).strip()
                    break
            except subprocess.CalledProcessError as err:
                logger.warn('Submission failure (attempt {}): {}'.format(
                    attempt + 1, err.output))
                logger.info('Retrying...')
        else:
            logger.error('Job submission failed after {} attempts'.format(
                attempt + 1))
            return

        sublog.write_bytes(output.encode('utf-8'))
        logger.info('PBS: {} is known to PBS as {}'.format(job.name, output))


class InteractiveRunner(Runner):
    def __init__(self):
        pass

    def submit(self, job):
        #job.output.parent.mkdir(parents=True, exist_ok=True)
        #logger.info('submitting (created {} if necessary)'.format(job.output.parent))
        logger.warn('submit is not implemented for InteractiveRunner yet')
        logger.warn('Use submit_parallel with the named argument N=1')
        return 0

    def describe(self):
        return 'Interactively'

    def submit_parallel(self, jobs, N=1):
        import time
        logger.info('Submitting {noj} jobs (with {N} to run in parallel)'.format(
            noj=len(jobs), N=N))
        time.sleep(3)

        if not jobs:
            return 0

        for job in jobs:
            if not job.output.parent.exists():
                job.output.parent.mkdir(parents=True, exist_ok=True)

        cmds = [job.command for job in jobs]
        outnames = [job.output for job in jobs]
        total_jobs = len(cmds)
        total_done = 0
        if not cmds: return # empty list

        def done(p):
            return p.poll() is not None
        def success(p):
            return p.returncode == 0
        def fail():
            sys.exit(1)

        max_task = N
        if max_task is None:
            max_task = 1
        processes = []
        while True:
            while cmds and len(processes) < max_task:
                task = cmds.pop()
                outbase = outnames.pop()
                stdoutname = '{}.stdout.log'.format(outbase).replace('.root','')
                stderrname = '{}.stderr.log'.format(outbase).replace('.root','')
                with open(stdoutname,'w') as sout, open(stderrname,'w') as serr:
                    processes.append((outbase,subprocess.Popen(task, shell=True,
                        stdout=sout, stderr=serr)))

            for n, p in processes:
                if done(p):
                    if success(p):
                        total_done += 1
                        logger.info('{} is done ({}/{})'.format(
                            n,total_done,total_jobs))
                        processes.remove((n,p))
                    else:
                        logger.error('Failure return code, check log for {}'.format(n))
                        fail()

            if not processes and not cmds:
                break
            else:
                time.sleep(0.05)

        return 0
