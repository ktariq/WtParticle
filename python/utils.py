import re
from PathResolver.PathResolver import FindCalibFile

_dsid_to_process = {}
_dsid_file = FindCalibFile('TopLoop/samplemeta.json')
import json
with open(_dsid_file,'r') as f:
    _j = json.load(f)
for proc, trees in _j.items():
    for tree in trees:
        dsid_range = tree["DSID_range"]
        for i in range(dsid_range[0],dsid_range[1]+1):
            _dsid_to_process[i] = tree["InitialState"]

import yaml
_dsid_yaml = FindCalibFile('WtParticle/process_to_dsid.yaml')
with open(_dsid_yaml,'r') as f:
    _yaml_dsid_table = yaml.load(f)


_datamatcher = re.compile('(.data[0-9]{2}.)')
_af2matcher = re.compile('(_a[0-9]{3})')
_fsmatcher  = re.compile('(_s[0-9]{3})')
_camp_dict = { 'r9364'  : 'MC16a',
               'r9781'  : 'MC16c',
               'r10201' : 'MC16d',
               'r10724' : 'MC16e'
}

def get_dsid_table():
    return _yaml_dsid_table

def get_simtype(name):
    if len(_af2matcher.findall(name)) > 0:
        return 'AFII'
    if len(_fsmatcher.findall(name)) > 0:
        return 'FS'
    if len(_datamatcher.findall(name)) > 0:
        return 'Data'
    return 'Unknown'

def get_campaign(name):
    for k, v in _camp_dict.items():
        if k in name:
            return v
    if len(_datamatcher.findall(name)) > 0:
        return 'Data'
    print('Cannot determine campaign from {}'.format(name))
    return 'Unknown'

def get_process(dsid):
    if 'data15' in dsid:
        return 'Data15'
    elif 'data16' in dsid:
        return 'Data16'
    elif 'data17' in dsid:
        return 'Data17'
    elif 'data18' in dsid:
        return 'Data18'
    else:
        this_dsid = int(dsid)
        return _dsid_to_process[this_dsid]

def get_all_systematics():
    fname = FindCalibFile('WtParticle/systematic_trees.v28.txt')
    with open(fname) as f:
        content = f.read().splitlines()
    return content
