from __future__ import print_function
from .batch import Job
from .utils import get_dsid_table
from pathlib2 import PosixPath
import yaml
import logging
logger = logging.getLogger(__name__)

def build_dsids(process_list):
    yaml_top = get_dsid_table()
    dsids = []
    for key, value in yaml_top['processes'].items():
        if key not in process_list:
            continue
        for entry in value:
            if isinstance(entry,list):
                for i in range(entry[0], entry[1]+1):
                    dsids.append(i)
            else:
                dsids.append(entry)
    return dsids

def build_jobs(rucio_datasets_path, config, trees, processes,
               output_root='.', enable_plevel=False, skipAFII=False,
               skip_campaigns=None, saver='SaverNone'):
    dsid_list = build_dsids(processes)
    for tree in trees:
        logger.info('For tree {}, DSIDs requested: {}'.format(tree, dsid_list))
    jobs = []
    datasets_path = PosixPath(rucio_datasets_path)
    for child in datasets_path.iterdir():
        if 'user.' in child.name and child.is_dir():
            individual_path = child.resolve()
            for tree in trees:
                job = Job(child.resolve(), config, tree, saver=saver,
                          output_root=output_root, enable_plevel=enable_plevel)
                if skipAFII and job.simtype == 'AFII':
                    continue
                if skip_campaigns and job.campaign in skip_campaigns:
                    continue
                if job.is_data and job.dsid in dsid_list:
                    jobs.append(job)
                if not job.is_data and int(job.dsid) in dsid_list:
                    jobs.append(job)
    return jobs
