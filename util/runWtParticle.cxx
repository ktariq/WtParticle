// ROOT
// #include <TError.h>

// TL and AL
//#include <WtParticle/Savers/SaverFullKinComb.h>
#include <WtParticle/WtParticleAlg.h>
#include <WtParticle/Externals/CLI11.hpp>
#include <WtParticle/Savers/SaverNone.h>

#include <TopLoop/Core/FileManager.h>
#include <TopLoop/Core/Job.h>
#include <TopLoop/Core/SampleMetaSvc.h>

// C++
#include <iostream>
#include <memory>

std::unique_ptr<TL::FileManager> build_fm_from_dir(
                                                   const std::string& dirname,
                                                   const wt2::Config* conf
                                                   ) {
  auto fm = std::make_unique<TL::FileManager>();
  
    fm->enableParticleLevel();
    fm->feedDir(dirname, conf->subsetInstructions());
  return fm;
}

TL::StatusCode runAlgo(const std::shared_ptr<wt2::Config>& config,
                       std::unique_ptr<TL::FileManager> fm,
                       const std::string& saverName,
                       const bool disableProgress) {
  if (fm->fileNames().empty()) {
    spdlog::get("runWtParticle")->error("Empty FileManager!");
    return TL::StatusCode::FAILURE;
  }
  TL::Job job;
  if (disableProgress) {
    job.disableProgressBar();
  }
 
  //job.setLoopType(TL::LoopType::RecoWithParticle);
  //  job.setLoopType(TL::LoopType::ParticleOnly); 
  job.setLoopType(TL::LoopType::ParticleAll); 
  
auto loopAlg = std::make_unique<wt2::WtParticleAlg>();
 if (saverName == "SaverNone") {
 //   auto saver = std::make_unique<wt2::SaverFullKinComb>(loopAlg.get());
   // loopAlg->setSaver(std::move(saver));
// }

 
    spdlog::get("runWtParticle")->warn("No Saver with name {}. Using 'None'", saverName);
    auto saver = std::make_unique<wt2::SaverNone>(loopAlg.get());
    loopAlg->setSaver(std::move(saver));
  }


  


 // loopAlg->setOutFileName(out_file_name);
 // loopAlg->setOutFileName(out_file_name);
  TL_CHECK(loopAlg->setConfig(config));
  TL_CHECK(job.setFileManager(std::move(fm)));
  TL_CHECK(job.setAlgorithm(std::move(loopAlg)));
  return job.run();
}

int main(int argc, char* argv[]) {
  spdlog::set_pattern("%n   %^[%=9l]%$   %v");
  auto runnerlog = spdlog::stdout_color_mt("runWtParticle");
  TL::StatusCode::enableFailure();

  CLI::App app("WtParticle");
  std::string inDir, outFile;
 // std::string inTree{"particleLevel"};
  std::string configFileName;
  std::string saverName = "SaverNone";
  bool debug, disableProgress;
  app.add_option("config", configFileName, "YAML Configuration File")
      ->required()
      ->check(CLI::ExistingFile);
  auto dirOpt =
      app.add_option("-d,--in-dir", inDir, "Path to directory containing files to process")
          ->check(CLI::ExistingDirectory);
//  auto txtOpt = app.add_option("--in-txt", inTxt, "Text file listing files to process")
  //                  ->check(CLI::ExistingFile);
//  app.add_option("-t,--tree-name", inTree,
  //               "Name of tree to process. "
    //             "Ignored if YAML config says run list of systematics!",
      //           true);
  app.add_option("-o,--out-file", outFile, "Name of output file")->required();
  app.add_option("--saver", saverName, "Name of saver class to use (default: FillKinComb)");
  app.add_flag("--debug", debug, "Print debugging statements");
  app.add_flag("--disable-progress", disableProgress, "disable progress bar");
 // app.add_flag("--enable-plevel", enablePartLevel, "enable particle level");

  CLI11_PARSE(app, argc, argv);

  if (debug) {
    spdlog::set_level(spdlog::level::debug);
  }

  auto config = std::make_shared<wt2::Config>(configFileName);

  if (dirOpt->count() > 0) {
    auto fm = build_fm_from_dir(inDir, config.get() );

    TL_CHECK(runAlgo(config, std::move(fm), saverName, disableProgress));
    return 0;
  }
/*
  else if (txtOpt->count() > 0) {
    auto fm = std::make_unique<TL::FileManager>();
    if (enablePartLevel) {
      fm->enableParticleLevel();
    }
    fm->setTreeName(inTree);
    fm->feedTxt(inTxt);
    TL_CHECK(runAlgo(config, std::move(fm), outFile, saverName, disableProgress));
    return 0;
  }
*/
  else {
    spdlog::get("runWtParticle")->error("Requires -d or --in-txt");
    std::cout << app.help() << std::endl;
    return 0;
  }
}
