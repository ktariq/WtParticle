TopLoop algorithm targeting particle level analysis.


To run:

runWtParticle conf_file -d / path to SgTopNtuple -o output.root

Exp:

runWtParticle default_config.yaml -d /eos/user/k/ktariq/v27/tW/user.csuster.mc16_13TeV.410646.PhPy8_Wt_DR_t.SGTOP1.e6552_s3126_r10724_p3629.ll.v27_out.root -o output.root


Note:

No need to specify -t (tree) or "--enable-plevel"


