#include <WtParticle/Config.h>
#include <yaml-cpp/yaml.h>

namespace detail {
template <typename T>
T set_or_default(const YAML::Node& node, const char* opt, const T& def) {
  if (not node[opt]) {
    return def;
  }
  else {
    return node[opt].as<T>();
  }
}

template <typename T>
void set(T& setthis, const YAML::Node& node) {
  if (node) {
    setthis = node.as<T>();
  }
  else {
    return;
  }
}
}  // namespace detail

wt2::Config::Config(const std::string& filename) : TL::Loggable("wt2::Config") {
  YAML::Node all_node = YAML::LoadFile(filename);
  auto top_node = all_node["WtParticle"];

  ////////////////////////////////////
  //////////////// file_management ///
  auto fm_node = top_node["file_management"];
  auto saus_node = fm_node["shuffle_and_use_subset"];
  for (const auto& ss_node : saus_node) {
    if (not ss_node.IsNull()) {
      auto dsid = ss_node["dsid"].as<unsigned int>();
      auto frac = ss_node["fraction"].as<float>();
      auto seed = ss_node["seed"].as<int>();
      auto camp = ss_node["campaign"].as<std::string>();
      TL::kCampaign campaign = TL::kCampaign::Unknown;
      if (camp == "MC16a")
        campaign = TL::kCampaign::MC16a;
      else if (camp == "MC16c")
        campaign = TL::kCampaign::MC16c;
      else if (camp == "MC16d")
        campaign = TL::kCampaign::MC16d;
      else if (camp == "MC16e")
        campaign = TL::kCampaign::MC16e;
      else if (camp == "MC16f")
        campaign = TL::kCampaign::MC16f;
      else if (camp == "MC15c")
        campaign = TL::kCampaign::MC15c;
      else {
        logger()->warn("Unknown campaign {}!", camp);
      }
      m_subsetInstructions.emplace_back(
          TL::FileManager::SubsetInstructions{dsid, campaign, frac, seed});
    }
  }

  ////////////////////////////////////
  //////////////////////// runoptions
  auto run_node = top_node["run_options"];

  m_do_btagSFs = detail::set_or_default(run_node, "btagSFs", false);
  m_save_MCfakes = detail::set_or_default(run_node, "save_MCfakes", false);
  m_scaled_lumi = detail::set_or_default(run_node, "scaled_lumi", 1000.0);

  if (run_node["disable_branches"]) {
    for (auto const val : run_node["disable_branches"]) {
      if (val.IsNull()) {
        continue;
      }
      m_disabled_branches.push_back(val.as<std::string>());
    }
  }

  if (not run_node["campaigns"]) {
    logger()->warn("No campaigns entry in config, using MC16a");
    m_campaigns.emplace_back(TL::kCampaign::MC16a);
  }
  else {
    for (auto const& camp : run_node["campaigns"]) {
      auto campstr = camp.as<std::string>();
      if (campstr == "MC16a")
        m_campaigns.emplace_back(TL::kCampaign::MC16a);
      else if (campstr == "MC16c")
        m_campaigns.emplace_back(TL::kCampaign::MC16c);
      else if (campstr == "MC16d")
        m_campaigns.emplace_back(TL::kCampaign::MC16d);
      else if (campstr == "MC16e")
        m_campaigns.emplace_back(TL::kCampaign::MC16e);
      else if (campstr == "MC16f")
        m_campaigns.emplace_back(TL::kCampaign::MC16f);
      else if (campstr == "MC15c")
        m_campaigns.emplace_back(TL::kCampaign::MC15c);
      else {
        logger()->warn("Unknown campaign {}!", campstr);
      }
    }
  }

  //////////////////////////////////////////
  //////////////////////// selection options
  auto selection_node = top_node["selection_options"];

  for (const auto& opt : selection_node["charge"]) {
    auto str_charge = opt.as<std::string>();
    if (str_charge == "OS")
      m_do_OS = true;
    else if (str_charge == "SS")
      m_do_SS = true;
    else {
      logger()->warn("Unknown charge {}!", str_charge);
    }
  }

  for (const auto& opt : selection_node["flavour"]) {
    auto str_flav = opt.as<std::string>();
    if (str_flav == "elmu")
      m_do_elmu = true;
    else if (str_flav == "mumu")
      m_do_mumu = true;
    else if (str_flav == "elel")
      m_do_elel = true;
    else {
      logger()->warn("Unknown flavour {}!", str_flav);
    }
  }

  m_jet_pt_min = detail::set_or_default(selection_node, "jet_pt_min", 20.0);

  m_loose_jet_pt_min = detail::set_or_default(selection_node, "loose_jet_pt_min", 20.0);
 // m_loop_type = detail::set_or_default(selection_node, "loop_type", "partOnly");
  m_minimax_divider =
      detail::set_or_default<float>(selection_node, "minimax_divider", 150.0);

  m_manual_el_eta_veto =
      detail::set_or_default<bool>(selection_node, "manual_el_eta_veto", false);

  m_max_njets = detail::set_or_default<unsigned int>(selection_node, "max_njets", 20);
/*
  auto btagwps = detail::set_or_default<std::string>(selection_node, "btagWP", "mv2c10_77");
  for (const auto& entry : TL::EDM::BTagWPDict) {
    if (btagwps == entry.second) {
      m_btagWP = entry.first;
      break;
    }
  }

  if (m_btagWP == TL::EDM::BTagWP::mv2c10_PC) {
    if (selection_node["btagBin"].IsNull()) {
      logger()->error("btagBin cannot be null for PC b-tagging");
    }
    auto btagbin =
        detail::set_or_default<std::string>(selection_node, "btagBin", "eff_77_70");
    for (const auto& entry : TL::EDM::BTagBinDict) {
      if (btagbin == entry.second) {
        m_btagBin = entry.first;
        break;
      }
    }
  }
*/

  m_save_with_flag = detail::set_or_default<bool>(selection_node, "save_with_flag", false);
  auto node_OF = selection_node["OF"];
  detail::set(m_OF_mll_divider, node_OF["mll_divider"]);
  detail::set(m_OF_under_divider_met_min, node_OF["mll_under_divider"]["met_minimum"]);
  detail::set(m_OF_over_divider_met_min, node_OF["mll_over_divider"]["met_minimum"]);
  auto node_SF = selection_node["SF"];
  detail::set(m_SF_reject_Z_range, node_SF["reject_Z_range"]);
  detail::set(m_SF_met_min, node_SF["met_minimum"]);
  detail::set(m_SF_mll_min, node_SF["mll_minimum"]);
  detail::set(m_SF_mll_divider, node_SF["mll_divider"]);
  if (node_SF["mll_under_divider"]) {
    detail::set(m_SF_under_divider_met_fac, node_SF["mll_under_divider"]["met_factor"]);
    detail::set(m_SF_under_divider_mll_fac, node_SF["mll_under_divider"]["mll_factor"]);
  }
  if (node_SF["mll_over_divider"]) {
    detail::set(m_SF_over_divider_met_fac, node_SF["mll_over_divider"]["met_factor"]);
    detail::set(m_SF_over_divider_mll_fac, node_SF["mll_over_divider"]["mll_factor"]);
    detail::set(m_SF_over_divider_sum_min, node_SF["mll_over_divider"]["sum_minimum"]);
  }

  ////////////////////////////////////////
  /////////////////////////// save options
  auto save_node = top_node["save_options"];
  if (save_node["excludes"]) {
    for (const auto& branch_name : save_node["excludes"]) {
      if (branch_name.IsNull()) continue;
      m_excludes.emplace_back(branch_name.as<std::string>());
    }
  }
  if (save_node["weight_excludes"]) {
    for (const auto& branch_name : save_node["weight_excludes"]) {
      if (branch_name.IsNull()) continue;
      m_weight_excludes.emplace_back(branch_name.as<std::string>());
    }
  }

  ////////////////////////////////////////////////////
  /////////////////////////// systematic_trees options
 
 auto systematics_node = top_node["systematics_options"];

  m_systematics_generator_variations =
      detail::set_or_default(systematics_node, "generator_variations", false);
  if (m_systematics_generator_variations) {
    if (systematics_node["variations_list"]) {
      for (const auto& varname : systematics_node["variations_list"]) {
        if (varname.IsNull()) {
          continue;
        }
        else if (varname.as<std::string>() == "PDFsetALL") {
          for (int i = 0; i < 10; ++i) {
            m_systematics_generator_variations_list.insert("PDFset=9090" +
                                                           std::to_string(i));
          }
          for (int i = 10; i < 31; ++i) {
            m_systematics_generator_variations_list.insert("PDFset=909" +
                                                           std::to_string(i));
          }
        }
        else {
          m_systematics_generator_variations_list.insert(varname.as<std::string>());
        }
      }
    }
  }
}

void wt2::Config::dump() const {
  logger()->info(
      "========================== Config Summary =====================================");
  if (do_btagSFs()) {
    logger()->info("Using b-tagging scale factors");
  }
  if (save_MCfakes()) {
    logger()->info("Saving fakes from MC");
  }

  logger()->info("Ignore events with jet multiplicity > {}", max_njets());

  std::string campcompat = "[";
  bool firstel = true;
  for (auto const& camp : campaigns()) {
    if (firstel) {
      firstel = false;
    }
    else {
      campcompat.append(", ");
    }
    campcompat.append(TL::SampleMetaSvc::get().getCampaignStr(camp));
  }
  campcompat.append("]");
  logger()->info("Campaign compatibility: {}", campcompat);

  if (do_OS()) {
    logger()->info("Saving opposite sign events");
  }
  if (do_SS()) {
    logger()->info("Saving same sign events");
  }
  if (do_elmu()) {
    logger()->info("Saving elmu events");
  }
  if (do_elel()) {
    logger()->info("Saving elel events");
  }
  if (do_mumu()) {
    logger()->info("Saving mumu events");
  }

  if (not disabled_branches().empty()) {
    logger()->info("The following SgTop ntuple branches are disabled:");
    logger()->info("(if your code is compiled to access them you will crash)");
    for (const auto& entry : disabled_branches()) {
      logger()->info(" - {}", entry);
    }
  }

  if (manual_el_eta_veto()) {
    logger()->info("Applying manual |eta| veto: [1.37, 1.52] and > 2.47 for electrons");
  }
/*
  for (const auto& entry : TL::EDM::BTagWPDict) {
    if (btagWP() == entry.first) {
      logger()->info("Using {} b-tagging WP", entry.second);
      break;
    }
  }

  if (btagWP() == TL::EDM::BTagWP::mv2c10_PC) {
    for (const auto& entry : TL::EDM::BTagBinDict) {
      if (btagBin() == entry.first) {
        logger()->info("Requring {} Pseudocontinuous b-tagging bin", entry.second);
        break;
      }
    }
  }
*/

  logger()->info("Lumi weights are scaled to: {} inverse picobarns", scaled_lumi());

  auto genvar_on_off = systematics_generator_variations() ? "ON" : "OFF";
  logger()->info("Generator variation access is turned {}. {}", genvar_on_off,
                 "Saved for nominal tW/ttbar samples");

  if (systematics_generator_variations()) {
    logger()->info("Saving the following generator weight variations:");
    for (auto const& entry : systematics_generator_variations_list()) {
      logger()->info(" - {}", entry);
    }
  }

//  logger()->info("2j2b will be subdivided at a minimaxmbl threshold of {} GeV",
  //               minimax_divider());
  logger()->info("minimum nominal jet pt: {} GeV", jet_pt_min());
  logger()->info("minimum loose jet pt: {} GeV", loose_jet_pt_min());
 // logger()->info("loop type", loop_type());

  logger()->info("MET/mll cuts summary:");
  if (save_with_flag()) {
    logger()->info(
        "Saving events which don't pass cuts! "
        "Use the \"passed_metmll_cuts\" variable to "
        "select only those which passed your cuts:!");
  }


  logger()->info("--- Opposite flavor (elmu) customizable cuts:");
  logger()->info("       mll divider               = {} GeV (separates for MET cut):",
                 OF_mll_divider());
  logger()->info(
      "       under divider MET minimum = {} GeV (if mll < {} GeV, "
      "use this MET cut)",
      OF_under_divider_met_min(), OF_mll_divider());
  logger()->info(
      "       over divider MET minimum  = {} GeV (if mll > {} GeV, "
      "use this MET cut)",
      OF_over_divider_met_min(), OF_mll_divider());
  logger()->info("--- Same flavor (elel, mumu) customizable cuts:");
  logger()->info("       Range of Z rejection: m_Z +/- {} GeV", SF_reject_Z_range());
  logger()->info("       MET minimum = {} GeV", SF_met_min());
  logger()->info("       mll minimum = {} GeV", SF_mll_min());
  logger()->info("       mll divider = {} GeV", SF_mll_divider());
  logger()->info("       for mll < {} GeV: ({}*MET < {}*mll) rejected)", SF_mll_divider(),
                 SF_under_divider_met_fac(), SF_under_divider_mll_fac());
  logger()->info("       for mll > {} GeV: (({}*MET + {}*mll) < {} GeV) rejected)",
                 SF_mll_divider(), SF_over_divider_met_fac(), SF_over_divider_mll_fac(),
                 SF_over_divider_sum_min());

  if (not excludes().empty()) {
    logger()->info("Exluding the following (regex based) output branches:");
    for (const auto& entry : excludes()) {
      logger()->info(" - {}", entry);
    }
  }
  if (not weight_excludes().empty()) {
    logger()->info("Exluding the following (regex based) weight output branches:");
    for (const auto& entry : weight_excludes()) {
      logger()->info(" - {}", entry);
    }
  }

  logger()->info(
      "===============================================================================");
}
