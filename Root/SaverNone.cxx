#include <TTree.h>
#include <WtParticle/Savers/SaverNone.h>

TL::StatusCode wt2::SaverNone::defineOutputVariables(TTree *tree) {
  (void)tree;  // silence compiler warning
  return TL::StatusCode::SUCCESS;
}

TL::StatusCode wt2::SaverNone::assignOutputVariables() { return TL::StatusCode::SUCCESS; }
