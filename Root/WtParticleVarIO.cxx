#include <WtParticle/WtParticleAlg.h>

#include <regex>

TL::StatusCode wt2::WtParticleAlg::init_additional_vars() {
  // where we would implement any SgTop ntuple variable access that is
  // not provided by default in TopLoop. TopLoop is currently
  // configured for SgTop ntuples though!
  return TL::StatusCode::SUCCESS;
}


  TL::StatusCode wt2::WtParticleAlg::defineOutputMetaVariables(TTree* tree) {
  tree->Branch("meta_yaml", &m_metaYAML);
  return TL::StatusCode::SUCCESS;
}





    
TL::StatusCode wt2::WtParticleAlg::defineOutputVariables(TTree* tree) {
  if ( tree == nullptr ) {
    return TL::StatusCode::FAILURE;
  }

  auto make_branch = [this,tree](const char* name, auto* v) -> void {
                       
                         for ( auto const& exclude : this->config()->excludes() ) {
                         std::regex r{exclude};
                         if ( std::regex_search(name,r) ) return;
                         }
                       
                       tree->Branch(name,v);
                     };

   make_branch("runNumber", &m_runNumber);
   make_branch("randomRunNumber", &m_randomRunNumber);
   make_branch("eventNumber", &m_eventNumber);

  make_branch("OS", &m_OS);
  make_branch("SS", &m_SS);
  make_branch("elmu", &m_elmu);
  make_branch("elel", &m_elel);
  make_branch("mumu", &m_mumu);
  
  make_branch("reg1j1b", &m_1j1b);
 /* make_branch("reg2j1b", &m_2j1b);
  make_branch("reg2j2b", &m_2j2b);
  make_branch("reg1j0b", &m_1j0b);
  make_branch("reg2j0b", &m_2j0b);
  make_branch("reg3j", &m_3j);
  make_branch("reg4j", &m_4j);
  make_branch("reg3j1b", &m_3j1b);
  make_branch("reg3j2b", &m_3j2b);
  make_branch("reg4j1b", &m_4j1b);
  make_branch("reg3pj", &m_3pj);
  make_branch("reg4j2b", &m_4j2b);
  make_branch("reg4pj2pb", &m_4pj2pb);
  make_branch("reg2j2bLmm", &m_2j2bLmm);
  make_branch("reg2j2bHmm", &m_2j2bHmm);
  */

  make_branch("pT_lep1", &m_pT_lep1);
  make_branch("pT_lep2", &m_pT_lep2);
  make_branch("pT_jet1", &m_pT_jet1);

  make_branch("eta_lep1", &m_eta_lep1);
  make_branch("eta_lep2", &m_eta_lep2);
  make_branch("eta_jet1", &m_eta_jet1);

  make_branch("phi_lep1", &m_phi_lep1);
  make_branch("phi_lep2", &m_phi_lep2);
  make_branch("phi_jet1", &m_phi_jet1);

  make_branch("E_lep1", &m_E_lep1);
  make_branch("E_lep2", &m_E_lep2);
  make_branch("E_jet1", &m_E_jet1);

  make_branch("mass_jet1", &m_mass_jet1);

  make_branch("met", &m_met);
  make_branch("eta_met", &m_eta_met);
  make_branch("phi_met", &m_phi_met);
 //  make_branch("sumet", &m_sumet);

  make_branch("njets", &m_njets);
  make_branch("nbjets", &m_nbjets);

  make_branch("pdgId_lep1", &m_pdgId_lep1);
  make_branch("pdgId_lep2", &m_pdgId_lep2);
  make_branch("charge_lep1", &m_charge_lep1);
  make_branch("charge_lep2", &m_charge_lep2);






 TL_CHECK(m_saver->defineOutputVariables(tree)); 
  return TL::StatusCode::SUCCESS;
}


/*
//////////////////I use the WtParticleAlgWeights ///////////////////
TL::StatusCode wt2::WtParticleAlg::defineOutputWeights(TTree* tree) {
  if ( tree == nullptr ) {
    return TL::StatusCode::FAILURE;
  }

  auto make_branch = [this,tree](const char* name, auto* v) -> void {
                ////////////////////
                         for ( auto const& exclude : this->config()->weight_excludes() ) {
                         std::regex r{exclude};
                         if ( std::regex_search(name,r) ) return;
                         }
                   /////////////////
                       tree->Branch(name,v);
                     };

  make_branch("weight_nominal",&m_weight_nominal);

  return TL::StatusCode::SUCCESS;
}
*/
