// WtLoop
#include <WtParticle/WtParticleAlg.h>

// fmt
#include <TopLoop/spdlog/fmt/fmt.h>

// boost
#include <boost/algorithm/string.hpp>

// C++
#include <cmath>
#include <regex>

TL::StatusCode wt2::WtParticleAlg::defineOutputWeights(TTree* tree) {
  if (tree == nullptr) {
    return TL::StatusCode::FAILURE;
  }

  auto make_branch = [this, tree](const char* name, auto* v) -> void {
    for (auto const& exclude : this->config()->weight_excludes()) {
      std::regex r(exclude);
      if (std::regex_search(name, r)) return;
    }
    tree->Branch(name, v);
  };

  make_branch("weight_nominal", &m_weight_nominal);
  make_branch("weight_nominal_raw", &m_weight_nominal_raw);
/*
  if (isMC() and isNominal() and (not isRel207())) {
    if (m_topPP8sample and config()->systematics_generator_variations()) {
      // by default do the radLo radHi weights which require a product
      make_branch("weight_sys_radLo", &m_weight_sys_radLo);
      make_branch("weight_sys_radHi", &m_weight_sys_radHi);
      // now do the variations asked for in the config
      for (const auto& variationstr : config()->systematics_generator_variations_list()) {
        m_weight_sys_genvar_list_branches[variationstr] = 0.0;
        std::string br_name = fmt::format("weight_sys_{}", variationstr);
        make_branch(br_name.c_str(), &m_weight_sys_genvar_list_branches.at(variationstr));
      }
    }

    if (TL::SampleMetaSvc::get().getSampleType(m_dsid) == TL::kSampleType::Nominal) {
      make_branch("weight_sys_jvt_UP", &m_weight_sys_jvt_UP);
      make_branch("weight_sys_jvt_DOWN", &m_weight_sys_jvt_DOWN);
      make_branch("weight_sys_pileup_UP", &m_weight_sys_pileup_UP);
      make_branch("weight_sys_pileup_DOWN", &m_weight_sys_pileup_DOWN);
      make_branch("weight_sys_leptonSF_EL_SF_Trigger_UP",
                  &m_weight_sys_leptonSF_EL_SF_Trigger_UP);
      make_branch("weight_sys_leptonSF_EL_SF_Trigger_DOWN",
                  &m_weight_sys_leptonSF_EL_SF_Trigger_DOWN);
      make_branch("weight_sys_leptonSF_EL_SF_Reco_UP",
                  &m_weight_sys_leptonSF_EL_SF_Reco_UP);
      make_branch("weight_sys_leptonSF_EL_SF_Reco_DOWN",
                  &m_weight_sys_leptonSF_EL_SF_Reco_DOWN);
      make_branch("weight_sys_leptonSF_EL_SF_ID_UP", &m_weight_sys_leptonSF_EL_SF_ID_UP);
      make_branch("weight_sys_leptonSF_EL_SF_ID_DOWN",
                  &m_weight_sys_leptonSF_EL_SF_ID_DOWN);
      make_branch("weight_sys_leptonSF_EL_SF_Isol_UP",
                  &m_weight_sys_leptonSF_EL_SF_Isol_UP);
      make_branch("weight_sys_leptonSF_EL_SF_Isol_DOWN",
                  &m_weight_sys_leptonSF_EL_SF_Isol_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_Trigger_STAT_UP",
                  &m_weight_sys_leptonSF_MU_SF_Trigger_STAT_UP);
      make_branch("weight_sys_leptonSF_MU_SF_Trigger_STAT_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_Trigger_STAT_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_Trigger_SYST_UP",
                  &m_weight_sys_leptonSF_MU_SF_Trigger_SYST_UP);
      make_branch("weight_sys_leptonSF_MU_SF_Trigger_SYST_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_Trigger_SYST_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_ID_STAT_UP",
                  &m_weight_sys_leptonSF_MU_SF_ID_STAT_UP);
      make_branch("weight_sys_leptonSF_MU_SF_ID_STAT_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_ID_STAT_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_ID_SYST_UP",
                  &m_weight_sys_leptonSF_MU_SF_ID_SYST_UP);
      make_branch("weight_sys_leptonSF_MU_SF_ID_SYST_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_ID_SYST_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_UP",
                  &m_weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
      make_branch("weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_UP",
                  &m_weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
      make_branch("weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_Isol_STAT_UP",
                  &m_weight_sys_leptonSF_MU_SF_Isol_STAT_UP);
      make_branch("weight_sys_leptonSF_MU_SF_Isol_STAT_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_Isol_STAT_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_Isol_SYST_UP",
                  &m_weight_sys_leptonSF_MU_SF_Isol_SYST_UP);
      make_branch("weight_sys_leptonSF_MU_SF_Isol_SYST_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_Isol_SYST_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_TTVA_STAT_UP",
                  &m_weight_sys_leptonSF_MU_SF_TTVA_STAT_UP);
      make_branch("weight_sys_leptonSF_MU_SF_TTVA_STAT_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_TTVA_STAT_DOWN);
      make_branch("weight_sys_leptonSF_MU_SF_TTVA_SYST_UP",
                  &m_weight_sys_leptonSF_MU_SF_TTVA_SYST_UP);
      make_branch("weight_sys_leptonSF_MU_SF_TTVA_SYST_DOWN",
                  &m_weight_sys_leptonSF_MU_SF_TTVA_SYST_DOWN);

      if (config()->do_btagSFs()) {
        // btagging eigenvars.. these are vectors loop over the array to
        // set the branch addresses.. make suffix "_up" or "_down".
        auto setup_bTag_eigenvar = [make_branch](const std::string& pref, auto& arr) {
          std::vector<std::string> splitstr;
          boost::split(splitstr, pref, [](char c) { return c == '_'; });
          bool isup = splitstr.back() == "up";
          splitstr.pop_back();
          std::string new_str;
          for (const auto& s : splitstr) {
            new_str.append(s);
            new_str.append(1, '_');
          }
          for (std::size_t i = 0; i < arr.size(); ++i) {
            std::string pref2 = fmt::format("{}{}", new_str, std::to_string(i));
            if (isup) {
              pref2.append("_up");
            }
            else {
              pref2.append("_down");
            }
            make_branch(pref2.c_str(), &arr[i]);
          }
        };
        if (config()->btagWP() == TL::EDM::BTagWP::mv2c10_77) {
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_77_eigenvars_B_up",
                              m_weight_sys_bTagSF_MV2c10_77_eigenvars_B_up);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_77_eigenvars_B_down",
                              m_weight_sys_bTagSF_MV2c10_77_eigenvars_B_down);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_77_eigenvars_C_up",
                              m_weight_sys_bTagSF_MV2c10_77_eigenvars_C_up);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_77_eigenvars_C_down",
                              m_weight_sys_bTagSF_MV2c10_77_eigenvars_C_down);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_77_eigenvars_Light_up",
                              m_weight_sys_bTagSF_MV2c10_77_eigenvars_Light_up);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_77_eigenvars_Light_down",
                              m_weight_sys_bTagSF_MV2c10_77_eigenvars_Light_down);

          // btagging float variables
          make_branch("weight_sys_bTagSF_MV2c10_77_extrapolation_up",
                      &m_weight_sys_bTagSF_MV2c10_77_extrapolation_up);
          make_branch("weight_sys_bTagSF_MV2c10_77_extrapolation_down",
                      &m_weight_sys_bTagSF_MV2c10_77_extrapolation_down);
          make_branch("weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_up",
                      &m_weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_up);
          make_branch("weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_down",
                      &m_weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_down);
        }
        else if (config()->btagWP() == TL::EDM::BTagWP::mv2c10_PC) {
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_Continuous_eigenvars_B_up",
                              m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_B_up);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_Continuous_eigenvars_B_down",
                              m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_B_down);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_Continuous_eigenvars_C_up",
                              m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_C_up);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_Continuous_eigenvars_C_down",
                              m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_C_down);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_Continuous_eigenvars_Light_up",
                              m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_Light_up);
          setup_bTag_eigenvar("weight_sys_bTagSF_MV2c10_Continuous_eigenvars_Light_down",
                              m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_Light_down);
        }
      }
    }
  
}
*/
  return TL::StatusCode::SUCCESS;
}

TL::StatusCode wt2::WtParticleAlg::assignOutputWeights() {
  if (isData()) {
    m_weight_nominal = 1.0;
    m_weight_nominal_raw = 1.0;
    return TL::StatusCode::SUCCESS;
  }


 m_weight_nominal = m_lumiWeight * PL_weight_mc() * PL_weight_pileup();
 m_weight_nominal_raw = m_weight_nominal / m_campaign_weight;
/*
  float weight_bTagSF_MV2c10_XX = 1.0;
  if (config()->do_btagSFs()) {
    if (config()->btagWP() == TL::EDM::BTagWP::mv2c10_77) {
      weight_bTagSF_MV2c10_XX = weight_bTagSF_MV2c10_77();
    }
    else if (config()->btagWP() == TL::EDM::BTagWP::mv2c10_PC) {
      weight_bTagSF_MV2c10_XX = weight_bTagSF_MV2c10_Continuous();
    }
    else {
      logger()->critical("Invalid b-tagging WP setting, 77 or PC only");
    }
  }

  float lepSF = weight_leptonSF();
  if (m_olderThanv28) {
    lepSF = weight_leptonSF_tight();
  }
  float lepSF_fixFac = 1.0;
  auto sgtopver = fileManager()->getSgTopNtupVersion();
  if (sgtopver == TL::kSgTopNtup::v25 && isNominal()) {
    lepSF_fixFac = (weight_globalLeptonTriggerSF() / weight_triggerSF_tight());
    lepSF *= lepSF_fixFac;
  }

  m_weight_nominal = (m_lumiWeight * weight_mc() * weight_pileup() * lepSF * weight_jvt() *
                      weight_bTagSF_MV2c10_XX);

  m_weight_nominal_raw = m_weight_nominal / m_campaign_weight;

  if (std::isnan(m_weight_nominal)) {
    logger()->critical("m_weight_nominal is nan, one of the ingredients is nan!");
    return TL::StatusCode::FAILURE;
  }

  if (isSystematic()) return TL::StatusCode::SUCCESS;

  if (m_topPP8sample && config()->systematics_generator_variations()) {
    const auto& nameMap = weightTool().generatorVariedWeightsNames();

    // Scale variation weight names are inconsistent between tW and
    // ttbar samples. Who knows why... quite annoying. We accommodate
    // for that here.
    std::string muRmuF_lo = "muR=05,muF=05";
    std::string muRmuF_hi = "muR=20,muF=20";
    if (nameMap.find("muR=050,muF=050") != nameMap.end()) {
      muRmuF_lo = "muR=050,muF=050";
      muRmuF_hi = "muR=200,muF=200";
    }

    // Calculate the normalization for the radLo and radHi weights.
    float radLo_norm = weightTool().sumOfVariation(muRmuF_lo) *
                       weightTool().sumOfVariation("Var3cDown") /
                       weightTool().generatorSumWeights();
    float radHi_norm = weightTool().sumOfVariation(muRmuF_hi) *
                       weightTool().sumOfVariation("Var3cUp") /
                       weightTool().generatorSumWeights();

    // The calculation of m_lumiWeight includes a division by
    // weightTool.generatorSumWeights(), used for the nominal weight
    // normalization; so for the weight based systematics we must
    // multiply it back out to get the correct normalization.

    m_weight_sys_radLo = m_lumiWeight * weightTool().generatorSumWeights() / radLo_norm *
                         weightTool().currentWeightOfVariation(muRmuF_lo) *
                         weightTool().currentWeightOfVariation("Var3cDown") / weight_mc() *
                         weight_pileup() * lepSF * weight_jvt() * weight_bTagSF_MV2c10_XX;

    m_weight_sys_radHi = m_lumiWeight * weightTool().generatorSumWeights() / radHi_norm *
                         weightTool().currentWeightOfVariation(muRmuF_hi) *
                         weightTool().currentWeightOfVariation("Var3cUp") / weight_mc() *
                         weight_pileup() * lepSF * weight_jvt() * weight_bTagSF_MV2c10_XX;

    for (const auto& varstr : config()->systematics_generator_variations_list()) {
      m_weight_sys_genvar_list_branches[varstr] =
          m_lumiWeight * weightTool().generatorSumWeights() /
          weightTool().sumOfVariation(varstr) *
          weightTool().currentWeightOfVariation(varstr) * weight_pileup() * lepSF *
          weight_jvt() * weight_bTagSF_MV2c10_XX;
    }
  }

  auto sampIsNominal =
      TL::SampleMetaSvc::get().getSampleType(m_dsid) == TL::kSampleType::Nominal;
  if (not isRel207() && sampIsNominal && m_topPP8sample) {
    m_weight_sys_jvt_UP = m_lumiWeight * weight_mc() * lepSF * weight_pileup() *
                          weight_bTagSF_MV2c10_XX * weight_jvt_UP();
    m_weight_sys_jvt_DOWN = m_lumiWeight * weight_mc() * lepSF * weight_pileup() *
                            weight_bTagSF_MV2c10_XX * weight_jvt_DOWN();

    m_weight_sys_pileup_UP = m_lumiWeight * weight_mc() * lepSF * weight_jvt() *
                             weight_bTagSF_MV2c10_XX * weight_pileup_UP();
    m_weight_sys_pileup_DOWN = m_lumiWeight * weight_mc() * lepSF * weight_jvt() *
                               weight_bTagSF_MV2c10_XX * weight_pileup_DOWN();

    float lepSFpref = m_lumiWeight * weight_mc() * weight_jvt() * weight_pileup() *
                      weight_bTagSF_MV2c10_XX;

    if (m_olderThanv28) {
      if (sgtopver == TL::kSgTopNtup::v23) {
        m_weight_sys_leptonSF_EL_SF_Trigger_UP =
            lepSFpref * weight_leptonSF_tight_EL_SF_Trigger_UP();
        m_weight_sys_leptonSF_EL_SF_Trigger_DOWN =
            lepSFpref * weight_leptonSF_tight_EL_SF_Trigger_DOWN();
        m_weight_sys_leptonSF_MU_SF_Trigger_STAT_UP =
            lepSFpref * weight_leptonSF_tight_MU_SF_Trigger_STAT_UP();
        m_weight_sys_leptonSF_MU_SF_Trigger_STAT_DOWN =
            lepSFpref * weight_leptonSF_tight_MU_SF_Trigger_STAT_DOWN();
        m_weight_sys_leptonSF_MU_SF_Trigger_SYST_UP =
            lepSFpref * weight_leptonSF_tight_MU_SF_Trigger_SYST_UP();
        m_weight_sys_leptonSF_MU_SF_Trigger_SYST_DOWN =
            lepSFpref * weight_leptonSF_tight_MU_SF_Trigger_SYST_DOWN();
      }
      else {
        m_weight_sys_leptonSF_EL_SF_Trigger_UP =
            lepSFpref * weight_globalLeptonTriggerSF_EL_Trigger_UP();
        m_weight_sys_leptonSF_EL_SF_Trigger_DOWN =
            lepSFpref * weight_globalLeptonTriggerSF_EL_Trigger_DOWN();
        m_weight_sys_leptonSF_MU_SF_Trigger_STAT_UP =
            lepSFpref * weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP();
        m_weight_sys_leptonSF_MU_SF_Trigger_STAT_DOWN =
            lepSFpref * weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN();
        m_weight_sys_leptonSF_MU_SF_Trigger_SYST_UP =
            lepSFpref * weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP();
        m_weight_sys_leptonSF_MU_SF_Trigger_SYST_DOWN =
            lepSFpref * weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN();
      }

      m_weight_sys_leptonSF_EL_SF_Reco_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_EL_SF_Reco_UP();
      m_weight_sys_leptonSF_EL_SF_Reco_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_EL_SF_Reco_DOWN();
      m_weight_sys_leptonSF_EL_SF_ID_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_EL_SF_ID_UP();
      m_weight_sys_leptonSF_EL_SF_ID_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_EL_SF_ID_DOWN();
      m_weight_sys_leptonSF_EL_SF_Isol_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_EL_SF_Isol_UP();
      m_weight_sys_leptonSF_EL_SF_Isol_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_EL_SF_Isol_DOWN();
      m_weight_sys_leptonSF_MU_SF_ID_STAT_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_ID_STAT_UP();
      m_weight_sys_leptonSF_MU_SF_ID_STAT_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_ID_STAT_DOWN();
      m_weight_sys_leptonSF_MU_SF_ID_SYST_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_ID_SYST_UP();
      m_weight_sys_leptonSF_MU_SF_ID_SYST_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_ID_SYST_DOWN();
      m_weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_ID_STAT_LOWPT_UP();
      m_weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_ID_STAT_LOWPT_DOWN();
      m_weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_ID_SYST_LOWPT_UP();
      m_weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_ID_SYST_LOWPT_DOWN();
      m_weight_sys_leptonSF_MU_SF_Isol_STAT_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_Isol_STAT_UP();
      m_weight_sys_leptonSF_MU_SF_Isol_STAT_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_Isol_STAT_DOWN();
      m_weight_sys_leptonSF_MU_SF_Isol_SYST_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_Isol_SYST_UP();
      m_weight_sys_leptonSF_MU_SF_Isol_SYST_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_Isol_SYST_DOWN();
      m_weight_sys_leptonSF_MU_SF_TTVA_STAT_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_TTVA_STAT_UP();
      m_weight_sys_leptonSF_MU_SF_TTVA_STAT_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_TTVA_STAT_DOWN();
      m_weight_sys_leptonSF_MU_SF_TTVA_SYST_UP =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_TTVA_SYST_UP();
      m_weight_sys_leptonSF_MU_SF_TTVA_SYST_DOWN =
          lepSFpref * lepSF_fixFac * weight_leptonSF_tight_MU_SF_TTVA_SYST_DOWN();
    }

    else {
      m_weight_sys_leptonSF_EL_SF_Trigger_UP =
          lepSFpref * weight_globalLeptonTriggerSF_EL_Trigger_UP();
      m_weight_sys_leptonSF_EL_SF_Trigger_DOWN =
          lepSFpref * weight_globalLeptonTriggerSF_EL_Trigger_DOWN();
      m_weight_sys_leptonSF_MU_SF_Trigger_STAT_UP =
          lepSFpref * weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP();
      m_weight_sys_leptonSF_MU_SF_Trigger_STAT_DOWN =
          lepSFpref * weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN();
      m_weight_sys_leptonSF_MU_SF_Trigger_SYST_UP =
          lepSFpref * weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP();
      m_weight_sys_leptonSF_MU_SF_Trigger_SYST_DOWN =
          lepSFpref * weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN();
      m_weight_sys_leptonSF_EL_SF_Reco_UP = lepSFpref * weight_leptonSF_EL_SF_Reco_UP();
      m_weight_sys_leptonSF_EL_SF_Reco_DOWN = lepSFpref * weight_leptonSF_EL_SF_Reco_DOWN();
      m_weight_sys_leptonSF_EL_SF_ID_UP = lepSFpref * weight_leptonSF_EL_SF_ID_UP();
      m_weight_sys_leptonSF_EL_SF_ID_DOWN = lepSFpref * weight_leptonSF_EL_SF_ID_DOWN();
      m_weight_sys_leptonSF_EL_SF_Isol_UP = lepSFpref * weight_leptonSF_EL_SF_Isol_UP();
      m_weight_sys_leptonSF_EL_SF_Isol_DOWN = lepSFpref * weight_leptonSF_EL_SF_Isol_DOWN();
      m_weight_sys_leptonSF_MU_SF_ID_STAT_UP =
          lepSFpref * weight_leptonSF_MU_SF_ID_STAT_UP();
      m_weight_sys_leptonSF_MU_SF_ID_STAT_DOWN =
          lepSFpref * weight_leptonSF_MU_SF_ID_STAT_DOWN();
      m_weight_sys_leptonSF_MU_SF_ID_SYST_UP =
          lepSFpref * weight_leptonSF_MU_SF_ID_SYST_UP();
      m_weight_sys_leptonSF_MU_SF_ID_SYST_DOWN =
          lepSFpref * weight_leptonSF_MU_SF_ID_SYST_DOWN();
      m_weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_UP =
          lepSFpref * weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP();
      m_weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN =
          lepSFpref * weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN();
      m_weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_UP =
          lepSFpref * weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP();
      m_weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN =
          lepSFpref * weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN();
      m_weight_sys_leptonSF_MU_SF_Isol_STAT_UP =
          lepSFpref * weight_leptonSF_MU_SF_Isol_STAT_UP();
      m_weight_sys_leptonSF_MU_SF_Isol_STAT_DOWN =
          lepSFpref * weight_leptonSF_MU_SF_Isol_STAT_DOWN();
      m_weight_sys_leptonSF_MU_SF_Isol_SYST_UP =
          lepSFpref * weight_leptonSF_MU_SF_Isol_SYST_UP();
      m_weight_sys_leptonSF_MU_SF_Isol_SYST_DOWN =
          lepSFpref * weight_leptonSF_MU_SF_Isol_SYST_DOWN();
      m_weight_sys_leptonSF_MU_SF_TTVA_STAT_UP =
          lepSFpref * weight_leptonSF_MU_SF_TTVA_STAT_UP();
      m_weight_sys_leptonSF_MU_SF_TTVA_STAT_DOWN =
          lepSFpref * weight_leptonSF_MU_SF_TTVA_STAT_DOWN();
      m_weight_sys_leptonSF_MU_SF_TTVA_SYST_UP =
          lepSFpref * weight_leptonSF_MU_SF_TTVA_SYST_UP();
      m_weight_sys_leptonSF_MU_SF_TTVA_SYST_DOWN =
          lepSFpref * weight_leptonSF_MU_SF_TTVA_SYST_DOWN();
    }

    if (config()->do_btagSFs()) {
      float bTagSFpref =
          m_lumiWeight * weight_mc() * weight_jvt() * weight_pileup() * lepSF;
      if (config()->btagWP() == TL::EDM::BTagWP::mv2c10_77) {
        // vector based weights
        for (std::size_t i = 0; i < m_weight_sys_bTagSF_MV2c10_77_eigenvars_B_up.size();
             ++i) {
          m_weight_sys_bTagSF_MV2c10_77_eigenvars_B_up[i] =
              weight_bTagSF_MV2c10_77_eigenvars_B_up().at(i) * bTagSFpref;
          m_weight_sys_bTagSF_MV2c10_77_eigenvars_B_down[i] =
              weight_bTagSF_MV2c10_77_eigenvars_B_down().at(i) * bTagSFpref;
        }
        for (std::size_t i = 0; i < m_weight_sys_bTagSF_MV2c10_77_eigenvars_C_up.size();
             ++i) {
          m_weight_sys_bTagSF_MV2c10_77_eigenvars_C_up[i] =
              weight_bTagSF_MV2c10_77_eigenvars_C_up().at(i) * bTagSFpref;
          m_weight_sys_bTagSF_MV2c10_77_eigenvars_C_down[i] =
              weight_bTagSF_MV2c10_77_eigenvars_C_down().at(i) * bTagSFpref;
        }
        for (std::size_t i = 0; i < m_weight_sys_bTagSF_MV2c10_77_eigenvars_Light_up.size();
             ++i) {
          m_weight_sys_bTagSF_MV2c10_77_eigenvars_Light_up[i] =
              weight_bTagSF_MV2c10_77_eigenvars_Light_up().at(i) * bTagSFpref;
          m_weight_sys_bTagSF_MV2c10_77_eigenvars_Light_down[i] =
              weight_bTagSF_MV2c10_77_eigenvars_Light_down().at(i) * bTagSFpref;
        }

        // float based weights
        m_weight_sys_bTagSF_MV2c10_77_extrapolation_up =
            bTagSFpref * weight_bTagSF_MV2c10_77_extrapolation_up();
        m_weight_sys_bTagSF_MV2c10_77_extrapolation_down =
            bTagSFpref * weight_bTagSF_MV2c10_77_extrapolation_down();
        m_weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_up =
            bTagSFpref * weight_bTagSF_MV2c10_77_extrapolation_from_charm_up();
        m_weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_down =
            bTagSFpref * weight_bTagSF_MV2c10_77_extrapolation_from_charm_down();
      }
      else if (config()->btagWP() == TL::EDM::BTagWP::mv2c10_PC) {
        // vector based weights
        for (std::size_t i = 0;
             i < m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_B_up.size(); ++i) {
          m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_B_up[i] =
              weight_bTagSF_MV2c10_Continuous_eigenvars_B_up().at(i) * bTagSFpref;
          m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_B_down[i] =
              weight_bTagSF_MV2c10_Continuous_eigenvars_B_down().at(i) * bTagSFpref;
        }
        for (std::size_t i = 0;
             i < m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_C_up.size(); ++i) {
          m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_C_up[i] =
              weight_bTagSF_MV2c10_Continuous_eigenvars_C_up().at(i) * bTagSFpref;
          m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_C_down[i] =
              weight_bTagSF_MV2c10_Continuous_eigenvars_C_down().at(i) * bTagSFpref;
        }
        for (std::size_t i = 0;
             i < m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_Light_up.size(); ++i) {
          m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_Light_up[i] =
              weight_bTagSF_MV2c10_Continuous_eigenvars_Light_up().at(i) * bTagSFpref;
          m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_Light_down[i] =
              weight_bTagSF_MV2c10_Continuous_eigenvars_Light_down().at(i) * bTagSFpref;
        }
      }
    }
  }
*/
  return TL::StatusCode::SUCCESS;
}
