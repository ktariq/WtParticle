// Analysis

 #include <MCTruthClassifier/MCTruthClassifierDefs.h>
 #include "MetaBuilder.h"

 #include <TopLoop/Core/SampleMetaSvc.h>
#include <TopLoop/json/json.hpp>

#include <WtParticle/WtParticleAlg.h>
#include <TopLoop/Core/SampleMetaSvc.h>
#include <TopLoop/EDM/Helpers.h>

 #include "TopLoop_version.h"
//#include "WtLoop_version.h"
#include <boost/algorithm/string.hpp>
#include <chrono>
#include <cmath>
#include <ctime>


wt2::WtParticleAlg::WtParticleAlg() {  m_logger = setupLogger("wt2::WtParticle");}
wt2::WtParticleAlg::~WtParticleAlg() { spdlog::drop("wt2::WtParticleAlg");}

TL::StatusCode wt2::WtParticleAlg::init() {
  TL_CHECK(TL::Algorithm::init());
  config()->dump();



m_finalState = std::make_unique<TL::EDM::FinalState>();
m_dsid         = get_dsid();
m_topPP8sample = TL::SampleMetaSvc::get().tWorTtbarPowPy8(m_dsid);

//char loop_type[] = "partOnly"; 
  if (isMC()) {
    auto find_camp =
        std::find(std::begin(config()->campaigns()), std::end(config()->campaigns()),
                  fileManager()->getCampaign());

    if (find_camp == std::end(config()->campaigns())) {
      logger()->critical(
          "This sample is associated with a campaign ({}) "
          "that you're missing in config file.",
          TL::SampleMetaSvc::get().getCampaignStr(fileManager()->getCampaign()));
      return TL::StatusCode::FAILURE;
    }

    m_totalNomWeights = weightTool().generatorSumWeights();
    m_lumiWeight =
        weightTool().luminosityWeight(config()->campaigns(), config()->scaled_lumi());
    m_campaign_weight = TL::SampleMetaSvc::get().getCampaignWeight(
        fileManager()->rucioDir(), config()->campaigns());
    logger()->info("DSID: {}, cross section: {} pb", m_dsid,
                   weightTool().sampleCrossSection());
  }
  else {
    m_lumiWeight = 1.0;
    m_totalNomWeights = 1.0;
    m_campaign_weight = 1.0;
  }


 logger()->info("Lumi weights will be calculated using SgTopNtup version: {}",
                 TL::SampleMetaSvc::get().ntupleVersionInUse());
  auto ntupversion = fileManager()->getSgTopNtupVersion();
  std::array<TL::kSgTopNtup, 3> old_list = {TL::kSgTopNtup::v23, TL::kSgTopNtup::v25,
                                            TL::kSgTopNtup::v27};
  if (std::find(old_list.begin(), old_list.end(), ntupversion) != old_list.end()) {
    m_olderThanv28 = true;
  }
  else {
    m_olderThanv28 = false;
  }
/*
  if (config()->btagWP() != TL::EDM::BTagWP::mv2c10_PC && !m_olderThanv28) {
    logger()->critical("Asking for fixed b-tagging but using v28+ ntuples, crashing");
    return TL::StatusCode::FAILURE;
  }
*/


  if (not config()->disabled_branches().empty()) {
    fileManager()->disableBranches(config()->disabled_branches());
  }

  return TL::StatusCode::SUCCESS;
}



////////////////////Setoutput/////////////////////////////

TL::StatusCode wt2::WtParticleAlg::setupOutput() {
  TL_CHECK(TL::Algorithm::setupOutput());

  std::unique_ptr<TFile> outfileptr(TFile::Open(m_outFileName.c_str(),"UPDATE"));
  m_outFile = std::move(outfileptr);



m_outTreeName = "WtParticle_"+fileManager()->treeName();

 if (m_outFile->GetListOfKeys()->Contains(m_outTreeName.c_str())) {
    logger()->warn(
        "{} already in the output file - "
        "Exiting gracefully.",
        m_outTreeName);
    std::exit(EXIT_SUCCESS);
  }
 
logger()->info("The saver you're using is: {}", m_saver->name());
 

  m_WtFlat = new TTree(m_outTreeName.c_str(), m_outTreeName.c_str());
  m_WtFlat->SetDirectory(m_outFile.get());
  TL_CHECK(defineOutputVariables(m_WtFlat));
  TL_CHECK(defineOutputWeights(m_WtFlat));



  

if (not m_outFile->GetListOfKeys()->Contains("WtParticle_meta")) {
    m_WtMeta = new TTree("WtParticle_meta", "WtParticle_meta");
    m_WtMeta->SetDirectory(m_outFile.get());
    TL_CHECK(defineOutputMetaVariables(m_WtMeta));
  }



 return TL::StatusCode::SUCCESS;
}

////////////////////////////Execute/////////////////////////

TL::StatusCode wt2::WtParticleAlg::execute() {
  TL_CHECK(TL::Algorithm::execute());

 resetEventProperties(); 
 //run number and event number 

 m_runNumber = PL_runNumber();
 m_randomRunNumber = PL_randomRunNumber();
 m_eventNumber = PL_eventNumber();


 
   // make sure we have at least 2 leptons for each event

 unsigned int nStartingLeptons = PL_el_pt().size() + PL_mu_pt().size();
  if (nStartingLeptons < 2) {
    return TL::StatusCode::SUCCESS;
  }
 

// maybe do some skipping based on config
 if ((config()->do_elel() || config()->do_elmu()) && !(config()->do_mumu())) {
    if (PL_el_pt().empty()) return TL::StatusCode::SUCCESS;
  }
  if ((config()->do_mumu() || config()->do_elmu()) && !(config()->do_elel())) {
    if (PL_mu_pt().empty()) return TL::StatusCode::SUCCESS;
  }



    // analyze the single top branches and fill EDM.
  TL_CHECK(addElectronsToFS(m_finalState.get()));
  TL_CHECK(addMuonsToFS(m_finalState.get()));
  TL_CHECK(addJetsToFS(m_finalState.get(), config()->jet_pt_min() * GeV, 2.5));
  TL_CHECK(addMissingETtoFS(m_finalState.get()));


  m_nbjets = 0;
  m_njets  = 0;
  for ( std::size_t i = 0; i < PL_jet_pt().size(); ++i ) {
    if ( PL_jet_pt().at(i) > 25000 ) {
      m_njets++;
    }
    if ( PL_jet_nGhosts_bHadron().at(i) > 0 ) {
      m_nbjets++;
    }
  m_1j1b = (m_njets == 1) && (m_nbjets == 1);

}



 if (isRel207()) {
    m_finalState->evaluateSelf(true,true); // true,true == sort leptons & manual promptness
 }
  else {
    m_finalState->evaluateSelf(true);  // true == sort leptons
  }



  if (not passObjectMultiplicity()) {
    return TL::StatusCode::SUCCESS;
  }




  bool passedmetmllcuts = passMETmllCuts();
  m_passed_metmll_cuts = passedmetmllcuts;
  if (not passedmetmllcuts && not config()->save_with_flag()) {
    return TL::StatusCode::SUCCESS;
  }


  else if (not passedmetmllcuts && config()->save_with_flag()) {
    float mll = fs()->leptonPairs().at(0).p4().M();
    if (fs()->leptonPairs().at(0).SF()) {
      if ((mll > (91.1876 - config()->SF_reject_Z_range()) * GeV) &&
          (mll < (91.1876 + config()->SF_reject_Z_range()) * GeV)) {
        return TL::StatusCode::SUCCESS;
      }
    }
  }


 TL_CHECK(assignOutputVariables());

// if (fs()->jets().size() > config()->max_njets()) {
  //  return TL::StatusCode::SUCCESS;
 // }

 TL_CHECK(assignOutputWeights());



 m_WtFlat->Fill();
 return TL::StatusCode::SUCCESS;
}



/////////////////Finish///////////////////////////
TL::StatusCode wt2::WtParticleAlg::finish() {
  TL_CHECK(TL::Algorithm::finish());

 char loop_type[] = "ParticleOnly";
// char loop_type[] = "ParticleAll";
// char loop_type[] = "RecoWithParticle";
 std::string campaignsList;
  for (const auto& c : config()->campaigns()) {
    if (not campaignsList.empty()) {
      campaignsList.append(",");
    }
    campaignsList.append(TL::SampleMetaSvc::get().getCampaignStr(c));
  }
 std::string tempMetaStr;

 if (not m_outFile->GetListOfKeys()->Contains("WtParticle_meta")) {
    logger()->info("Preparing meta data for serializing to ROOT file");
    std::time_t time_now = std::time(nullptr);
    std::string current_time(std::ctime(&time_now));
    current_time.erase(std::remove(current_time.begin(), current_time.end(), '\n'),
                       current_time.end());

    std::vector<std::string> campsvec;
    boost::split(campsvec, campaignsList, boost::is_any_of(","));
    if (isData()) campsvec = {"Data"};

    wt2::detail::MetaBuilder metaBuilder;
    metaBuilder.add_key_value("date", current_time);
    metaBuilder.add_key_value("sgtopntuple", fileManager()->rucioDir());
    metaBuilder.add_key_value("TopLoop_branch", TOPLOOP_GIT_BRANCH);
    metaBuilder.add_key_value("TopLoop_hash", TOPLOOP_GIT_COMMIT_HASH);
    metaBuilder.add_key_value("dsid", m_dsid);
    metaBuilder.add_key_value("initial_state",
                              TL::SampleMetaSvc::get().getInitialStateStr(m_dsid));
    metaBuilder.add_key_value("loop_type", loop_type);
    metaBuilder.add_key_value("generator",
                              TL::SampleMetaSvc::get().getGeneratorStr(m_dsid));
    metaBuilder.add_key_value(
        "campaign", TL::SampleMetaSvc::get().getCampaignStr(fileManager()->rucioDir()));
    metaBuilder.add_key_flow("campaign_compatibility", campsvec);
    metaBuilder.add_key_value("year", TL::SampleMetaSvc::get().getYear(m_runNumber));
    metaBuilder.add_key_value("isAFII",
                              TL::SampleMetaSvc::get().isAFII(fileManager()->rucioDir()));
    metaBuilder.add_key_value("lumi", static_cast<double>(config()->scaled_lumi()));
    metaBuilder.add_key_value("lumi_weight", static_cast<double>(m_lumiWeight));
    metaBuilder.add_key_value("campaign_weight", m_campaign_weight);
    metaBuilder.add_key_value("total_nominal_weights",
                              static_cast<double>(m_totalNomWeights));
    metaBuilder.add_key_value("cross_section",
                              static_cast<double>(weightTool().sampleCrossSection()));
    metaBuilder.add_key_value("raw_cross_section",
                              static_cast<double>(weightTool().sampleRawCrossSection()));
    metaBuilder.add_key_value("kfactor", static_cast<double>(weightTool().sampleKfactor()));
    metaBuilder.add_key_value("jet_pt_min", static_cast<double>(config()->jet_pt_min()));
  //  metaBuilder.add_key_value("loose_jet_pt_min",
   //                           static_cast<double>(config()->loose_jet_pt_min()));
    tempMetaStr = metaBuilder.to_str();
    m_metaYAML = tempMetaStr;
    m_WtMeta->Fill();
  }
  m_outFile->Write();
  m_outFile->Close();
 logger()->info(
      "Done... tree {} written to file {}."
      " (and MCNP file if requested).",
      m_outTreeName, m_outFileName);

  if (isMC()) {
    logger()->warn(
        "This output is ONLY compatible in combination "
        "with the following campaigns:");
    for (const auto& c : config()->campaigns()) {
      logger()->warn("- {}", TL::SampleMetaSvc::get().getCampaignStr(c));
    }
  }
 
 return TL::StatusCode::SUCCESS;
}



   bool wt2::WtParticleAlg::passObjectMultiplicity() const {
  if (fs()->leptonPairs().empty()) {
    return false;
  }

  if (!(config()->do_elel()) && fs()->leptonPairs().at(0).elel()) {
    return false;
  }
  if (!(config()->do_mumu()) && fs()->leptonPairs().at(0).mumu()) {
    return false;
  }
  if (!(config()->do_elmu()) && fs()->leptonPairs().at(0).elmu()) {
    return false;
  }
  if (!(config()->do_SS()) && fs()->leptonPairs().at(0).SS()) {
    return false;
  }
  if (!(config()->do_OS()) && fs()->leptonPairs().at(0).OS()) {
    return false;
  }

 if (fs()->leptons().size() > 2) {
    logger()->debug("Skipping event with 3rd lepton pt = {} GeV",
                    fs()->leptons().at(2).pt() * toGeV);
    return false;
  }


 if (fs()->jets().empty()) {
    return false;
  }

  return true;
}


  bool wt2::WtParticleAlg::passMETmllCuts() const {
  if (fs()->leptonPairs().empty()) return false;
  const TL::EDM::LeptonPair& lepPair = fs()->leptonPairs().at(0);  // pair
  if (lepPair.firstIdx() != 0 || lepPair.secondIdx() != 1) {
    logger()->error("Lepton pair indices are bad: {}, {}", lepPair.firstIdx(),
                    lepPair.secondIdx());
    std::exit(EXIT_FAILURE);
  }

  auto metf = fs()->MissingET().pt();  // met
  auto mll = lepPair.p4().M();         // pair inv mass
 
////////////////////////////////////////////////////////////
//  // if opposite flavor
 if (lepPair.OF()) {
    if (mll < config()->OF_mll_divider() * GeV) {
      if (metf < config()->OF_under_divider_met_min() * GeV) {
        return false;
      }
}


else {
      if (metf < config()->OF_over_divider_met_min() * GeV) {
        return false;
      }
    }  // if mll over divider

  }  
///////////////////////////////////////////////////////////
//  // if same flavor
else {
 if ((mll > (91.1876 - config()->SF_reject_Z_range()) * GeV) &&
        (mll < (91.1876 + config()->SF_reject_Z_range()) * GeV)) {
      return false;
    }
    if (metf < config()->SF_met_min() * GeV) {
      return false;
    }
    if (mll < config()->SF_mll_min() * GeV) {
      return false;
    }
    if (mll < config()->SF_mll_divider() * GeV) {
      if ((config()->SF_under_divider_met_fac() * metf) <
          (config()->SF_under_divider_mll_fac() * mll)) {
        return false;
      }
    }
    else {
      if ((config()->SF_over_divider_mll_fac() * mll +
           config()->SF_over_divider_met_fac() * metf) <
          (config()->SF_over_divider_sum_min() * GeV)) {
        return false;
      }
    }

  }  // end if same flavor*/
  return true;
}



 /////////////////////////Assign output variable//////////////////////////////////////
  TL::StatusCode wt2::WtParticleAlg::assignOutputVariables() {
  const TL::EDM::Lepton& lep1 = fs()->leptons().at(0);
  const TL::EDM::Lepton& lep2 = fs()->leptons().at(1);
  const TL::EDM::LeptonPair& lepPair = fs()->leptonPairs().at(0);
 const TL::EDM::Jet& jet1 = fs()->jets().at(0);
  const TL::EDM::MissingET& met = fs()->MissingET();

// std::size_t fwdJetIdx = fs()->mostForwardJetIdx();
// const TL::EDM::Jet& jetF = fs()->jets().at(fwdJetIdx);


  m_OS = lepPair.OS();
  m_SS = lepPair.SS();
  m_elmu = lepPair.elmu();
  m_elel = lepPair.elel();
  m_mumu = lepPair.mumu();
  m_met = met.pt() * toGeV;
  m_phi_met = met.phi();
  m_eta_met = met.phi();

  m_njets = fs()->jets().size();
// bool multjets = (m_njets > 1);
//  m_nloosejets = fs()->looseJets().size();

 //b-jet

/*
for ( std::size_t i = 0; i < PL_jet_pt().size(); ++i ) {
    
m_nbjets = PL_jet_nGhosts_bHadron().at(i);

  }
  m_1j1b = (m_njets == 1) && (m_nbjets == 1);
*/

/*  
m_2j1b = (m_njets == 2) && (m_nbjets == 1);
  m_2j2b = (m_njets == 2) && (m_nbjets == 2);
  m_1j0b = (m_njets == 1) && (m_nbjets == 0);
  m_2j0b = (m_njets == 2) && (m_nbjets == 0);
  m_3j = (m_njets == 3);
  m_4j = (m_njets == 4);
  m_3j1b = (m_njets == 3) && (m_nbjets == 1);
  m_3j2b = (m_njets == 3) && (m_nbjets == 2);
  m_4j1b = (m_njets == 4) && (m_nbjets == 1);
  m_3pj = (m_njets >= 3);
 
 m_4j2b = (m_njets == 4) && (m_nbjets == 2);
  m_4pj2pb = (m_njets >= 4) && (m_nbjets >= 2);

  if (m_njets > config()->max_njets()) {
    return TL::StatusCode::SUCCESS;
  }



  TL::EDM::Jet jet2;
if (not multjets) {
    jet2.p4().SetPtEtaPhiM(0, 0, 0, 0);
  }
  else {
    jet2 = fs()->jets().at(1);
  }

  TL::EDM::Jet jetL1;
  if (fs()->looseJets().empty()) {
    jetL1.p4().SetPtEtaPhiM(0, 0, 0, 0);
  }
  else {
    jetL1 = fs()->looseJets().at(0);
  }

*/


///////////////////////////////////////////////
 

TL::EDM::Jet alljets;
  alljets.p4().SetPtEtaPhiM(0, 0, 0, 0);
  for (const auto& jet : fs()->jets()) {
    alljets.p4() += jet.p4();
  }
/////////////////////////////////////////
 
  m_pT_lep1 = lep1.pt() * toGeV;
  m_pT_lep2 = lep2.pt() * toGeV;
 // m_pT_jetL1 = jetL1.pt() * toGeV;
//  m_pT_jetF = jetF.pt() * toGeV;
  m_pT_jet1 = jet1.pt() * toGeV;

  m_eta_lep1 = lep1.eta();
  m_eta_lep2 = lep2.eta();
  m_eta_jet1 = jet1.eta();
//  m_eta_jet2 = jet2.eta();
//  m_eta_jetL1 = jetL1.eta();
//  m_eta_jetF = jetF.eta();

 
  m_phi_lep1 = lep1.phi();
  m_phi_lep2 = lep2.phi();
  m_phi_jet1 = jet1.phi();

//  m_phi_jet2 = jet2.phi();
//  m_phi_jetL1 = jetL1.phi();
//  m_phi_jetF = jetF.phi();

  m_E_lep1 = lep1.E() * toGeV;
  m_E_lep2 = lep2.E() * toGeV;
  m_E_jet1 = jet1.E() * toGeV;

 // m_E_jet2 = jet2.E() * toGeV;
//  m_E_jetL1 = jetL1.E() * toGeV;
///  m_E_jetF = jetF.E() * toGeV;

  m_mass_jet1 = jet1.mass() * toGeV;
//  m_mass_jet2 = jet2.mass() * toGeV;
//  m_mass_jetL1 = jetL1.mass() * toGeV;
//  m_mass_jetF = jetF.mass() * toGeV;


  m_pdgId_lep1 = lep1.pdgId();
  m_pdgId_lep2 = lep2.pdgId();
  m_charge_lep1 = lep1.charge();
  m_charge_lep2 = lep2.charge();

 




 // detailed jet tagging information
  
 // m_mv2c10_jet1 = jet1.mv2c10();
/*  
m_mv2c10_jet2 = (multjets) ? jet2.mv2c10() : 0;
  m_mv2c10_jetF = jetF.mv2c10();
  m_DL1_jetF = jetF.DL1();

  m_truthflav_jet1 = -1;
  m_truthPartonLabel_jet1 = -1;
  m_isTrueHS_jet1 = false;
  m_truthflav_jet2 = -1;
  m_truthPartonLabel_jet2 = -1;
  m_isTrueHS_jet2 = false;
  if (isMC()) {
    m_truthflav_jet1 = jet1.truthflav();
    m_truthPartonLabel_jet1 = jet1.truthPartonLabel();
    m_isTrueHS_jet1 = static_cast<bool>(jet1.isTrueHS());
    if (multjets) {
      m_truthflav_jet2 = jet2.truthflav();
      m_truthPartonLabel_jet2 = jet2.truthPartonLabel();
      m_isTrueHS_jet2 = static_cast<bool>(jet2.isTrueHS());
    }
  }

  m_psuedoContTagBin_jet1 = jet1.tagWeightBin_MV2c10_Continuous();
  m_psuedoContTagBin_jet2 = (multjets) ? jet2.tagWeightBin_MV2c10_Continuous() : 0;
  m_psuedoContTagBin_jetF = jetF.tagWeightBin_MV2c10_Continuous();
*/

   TL_CHECK(m_saver->assignOutputVariables());
  
/*
  m_eta_jetAvg = std::accumulate(fs()->jets().begin(), fs()->jets().end(), 0.0f,
                                 [](float lhs, const auto& j) { return lhs + j.aeta(); }) /
                 static_cast<float>(m_njets);


  m_mT_jet1met = TL::EDM::transverseMass(jet1, met) * toGeV;
  m_mT_lep1met = TL::EDM::transverseMass(lep1, met) * toGeV;
  m_mT_lep2met = TL::EDM::transverseMass(lep2, met) * toGeV;

  m_deltaR_lep1lep2jetC_jetF = 0;
  if (m_njets == 2) {
    auto centralJetIdx = std::abs(static_cast<int>(fwdJetIdx) - 1);
    m_deltaR_lep1lep2jetC_jetF =
        TL::EDM::deltaR({lep1, lep2, jetF}, {fs()->jets().at(centralJetIdx)});
  }

  if (multjets) {
    TL::EDM::PhysicsSystem lep1jet1({lep1, jet1});
    TL::EDM::PhysicsSystem lep2jet1({lep2, jet1});
    TL::EDM::PhysicsSystem lep1jet2({lep1, jet2});
    TL::EDM::PhysicsSystem lep2jet2({lep2, jet2});
    float m11 = lep1jet1.p4().M();
    float m12 = lep2jet1.p4().M();
    float m21 = lep1jet2.p4().M();
    float m22 = lep2jet2.p4().M();
    m_minimaxmbl = std::max(std::min(m11, m22), std::min(m12, m21)) * toGeV;
  }
  else {
    m_minimaxmbl = 0.0;
  }
  if (m_2j2b) {
    m_2j2bLmm = m_minimaxmbl < config()->minimax_divider();
    m_2j2bHmm = m_minimaxmbl >= config()->minimax_divider();
  }
  else {
    m_2j2bLmm = false;
    m_2j2bHmm = false;
  }



 */     

   return TL::StatusCode::SUCCESS;
}

  
 

 /////////////////////////////add electron to FS  ////////////////////////////////////////////////

   TL::StatusCode wt2::WtParticleAlg::addElectronsToFS(TL::EDM::FinalState* fs) const {
  for (std::size_t i = 0; i < PL_el_pt().size(); ++i) {
  
  TL::EDM::Electron lep;
  lep.p4().SetPtEtaPhiM(PL_el_pt().at(i), PL_el_eta().at(i), PL_el_phi().at(i), 0.510999);
  
  if (lep.pt() < 25 * GeV) continue;
  if (lep.aeta() > 2.5) continue;

lep.set_charge(PL_el_charge().at(i));
lep.set_e_branch(PL_el_e().at(i));
  
 fs->addElectron(lep);
  }
  return TL::StatusCode::SUCCESS;
}

 ///////////////////////////// //Add Muons to FS//////////////////////////////////////////////////

  TL::StatusCode wt2::WtParticleAlg::addMuonsToFS(TL::EDM::FinalState* fs) const {
  for (std::size_t i = 0; i < PL_mu_pt().size(); ++i) {
    
    TL::EDM::Muon lep;
    lep.p4().SetPtEtaPhiM(PL_mu_pt().at(i), PL_mu_eta().at(i), PL_mu_phi().at(i), 105.6583745);
    
  if (lep.pt() < 25 * GeV) continue;
  if (lep.aeta() > 2.5) continue; 
                    
lep.set_charge(PL_mu_charge().at(i));
lep.set_e_branch(PL_mu_e().at(i));
 
fs->addMuon(lep);
 }
  return TL::StatusCode::SUCCESS;
}


 //////////////////////////////////////////////////Add Jest to FS//////////////////////////////////////////
   TL::StatusCode wt2::WtParticleAlg::addJetsToFS(TL::EDM::FinalState* fs, const float min_pt,
                                           const float max_eta) const {

for (std::size_t i = 0; i < PL_jet_pt().size(); ++i) {
    TL::EDM::Jet jet;
    jet.p4().SetPtEtaPhiE(PL_jet_pt().at(i), PL_jet_eta().at(i), PL_jet_phi().at(i), PL_jet_e().at(i));
  
  if (jet.aeta() >  max_eta) continue;  

    //  jet.set_mv2c10(PL_jet_nGhosts_bHadron().at(i));



  if (jet.pt() >= min_pt) {
      
   fs->addJet(jet);
    }

else {
      continue;
    }



}


return TL::StatusCode::SUCCESS;
}
 
/////////////////////////////////////////// //Add MissingET to FS/////////////////////////////////
  TL::StatusCode wt2::WtParticleAlg::addMissingETtoFS(TL::EDM::FinalState* fs) const {
fs->MissingET().p4().SetPtEtaPhiM(PL_met_met(), 0.0, PL_met_phi(), 0.0);
return TL::StatusCode::SUCCESS;
      
}


