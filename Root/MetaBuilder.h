/** @file MetaBuilder.h
 *  @brief MetaBuilder class header
 *  @class MetaBuilder
 *  @brief Build YAML style metadata output
 *
 *  A class to build a YAML style output string containing information
 *  about a dataset.
 *
 *  @author Douglas Davis, <ddavis@cern.ch>
 */

#ifndef WtParticle_MetaBuilder_h
#define WtParticle_MetaBuilder_h

#include <yaml-cpp/yaml.h>

namespace wt2 {
namespace detail {
class MetaBuilder {
 private:
  YAML::Emitter m_emitter{};

 public:
  MetaBuilder() { m_emitter << YAML::BeginMap; }
  virtual ~MetaBuilder() = default;

  MetaBuilder(const MetaBuilder&) = delete;
  MetaBuilder(MetaBuilder&&) = delete;
  MetaBuilder& operator=(const MetaBuilder&) = delete;
  MetaBuilder& operator=(MetaBuilder&&) = delete;

  template <typename T1, typename T2>
  void add_key_value(const T1& key, const T2& value);

  template <typename T1, typename T2>
  void add_key_flow(const T1& key, const std::vector<T2>& values);

  inline const std::string to_str();
};
}  // namespace detail
}  // namespace wt2

template <typename T1, typename T2>
inline void wt2::detail::MetaBuilder::add_key_value(const T1& key, const T2& value) {
  m_emitter << YAML::Key << key;
  m_emitter << YAML::Value << value;
}

template <typename T1, typename T2>
void wt2::detail::MetaBuilder::add_key_flow(const T1& key, const std::vector<T2>& values) {
  m_emitter << YAML::Key << key;
  m_emitter << YAML::Flow;
  m_emitter << YAML::BeginSeq;
  for (auto const& entry : values) {
    m_emitter << entry;
  }
  m_emitter << YAML::EndSeq;
}

inline const std::string wt2::detail::MetaBuilder::to_str() {
  m_emitter << YAML::EndMap;
  return std::string(m_emitter.c_str());
}

#endif
