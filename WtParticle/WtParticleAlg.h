/** @file  WtParticleAlg.h
 *  @brief WtParticleAlg class header
 *  @class WtParticleAlg
 *  @brief TopLoop based algorith for Wt analysis (PL)
 *
 *  A class to process SingleTop particle level ntuples
 *
 *  @author Douglas Davis, <ddavis@cern.ch>
 */

#ifndef WtParticle_WtParticleAlg_h
#define WtParticle_WtParticleAlg_h

// TopLoop
#include <TopLoop/Core/Algorithm.h>
#include <TopLoop/EDM/FinalState.h>
#include <TopLoop/Core/SampleMetaSvc.h>

// WtParticle
#include <WtParticle/WtParticleAlgVars.h>
 #include <WtParticle/Config.h>
#include <WtParticle/Saver.h>

// ROOT
#include <TFile.h>

namespace wt2 {

  class WtParticleAlg : public TL::Algorithm, public wt2::WtParticleAlgVars {

  private:
    std::string m_outFileName{"unnamed_file.root"};
    std::string m_outTreeName{"unnamed_tree"};
   bool m_topPP8sample{false};
  bool m_olderThanv28{false};
  float m_campaign_weight{1.0};
  bool m_shouldSaveFakes{false};

  std::unique_ptr<wt2::Saver> m_saver;
 
 std::unique_ptr<TFile> m_outFile{nullptr};
  TTree* m_WtFlat{nullptr};
  TTree* m_WtMeta{nullptr};

  std::unique_ptr<TFile> m_outFile_fake{nullptr};
  TTree* m_WtFlat_fake{nullptr};
  TTree* m_WtMeta_fake{nullptr};

  std::unique_ptr<TL::EDM::FinalState> m_finalState{nullptr};
  std::shared_ptr<wt2::Config> m_config{nullptr};
  std::shared_ptr<spdlog::logger> m_loc_logger{nullptr};

 
 

public:
 const TL::EDM::FinalState* fs() const { return m_finalState.get(); }
 const wt2::Config* config() const { return m_config.get(); }
  public:
    WtParticleAlg();
    virtual ~WtParticleAlg();
    WtParticleAlg(WtParticleAlg&&) = default;
    WtParticleAlg& operator=(const WtParticleAlg&) = default;
    WtParticleAlg& operator=(WtParticleAlg&&) = default;


     TL::StatusCode setConfig(const std::shared_ptr<wt2::Config>& cfg) {
    if (cfg == nullptr) {
      logger()->critical("config pointer is null!");
      return TL::StatusCode::FAILURE;
    }
    m_config = cfg;
    return TL::StatusCode::SUCCESS;
  }

  void setSaver(std::unique_ptr<wt2::Saver> saver) { m_saver = std::move(saver); }

    /// set the output ROOT file name
    void setOutFileName(const std::string& name);

    TL::StatusCode init()        final;
    TL::StatusCode setupOutput() final;
    TL::StatusCode execute()     final;
    TL::StatusCode finish()      final;

  private:
    // additional variable setup
    TL::StatusCode init_additional_vars();
//   bool passTriggerRequirements(const bool logit = true) const;
  bool passObjectMultiplicity() const;
  bool passMETmllCuts() const;
  TL::StatusCode defineOutputMetaVariables(TTree* tree);
    TL::StatusCode assignOutputWeights();

    /// sets up output weight branches
    TL::StatusCode defineOutputWeights(TTree* tree);
    /// sets up the output branches
    TL::StatusCode defineOutputVariables(TTree* tree);
    TL::StatusCode assignOutputVariables();

    /// reset any variables that are event dependent
    void resetEventProperties() { m_finalState->reset(); }

// void printFiredTriggers(spdlog::level::level_enum loglevel = spdlog::level::info) const;
//  void printTrigMatches(spdlog::level::level_enum loglevel = spdlog::level::info) const;

  const std::shared_ptr<spdlog::logger>& loc_logger() const { return m_loc_logger; }
  std::shared_ptr<spdlog::logger>& loc_logger() { return m_loc_logger; }
  

  private:
TL::StatusCode addElectronsToFS(TL::EDM::FinalState* fs) const; 
TL::StatusCode addMuonsToFS(TL::EDM::FinalState* fs) const;
  
TL::StatusCode addJetsToFS(TL::EDM::FinalState* fs, const float min_pt = 0.0,
                             const float max_eta = 1000) const;
 TL::StatusCode addMissingETtoFS(TL::EDM::FinalState* fs) const;

friend class Saver;
  

};

//  inline void WtParticleAlg::setOutFileName(const std::string& name) {
  //  m_outFileName = name;
 // }

}

#endif
