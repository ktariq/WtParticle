/** @file SaverNone.h
 *  @brief SaverNone class header
 *  @class SaverNone
 *  @brief Skeleton class for saving no extra variables
 *
 *  Use this saver for not saving any variables beyond the default in WtLoopAlg
 *
 *  @author Doug Davis <ddavis@cern.ch>
 */

#ifndef WTLOOP_SAVERNONE_H
#define WTLOOP_SAVERNONE_H

#include <WtParticle/Saver.h>

namespace wt2 {
class SaverNone : public Saver {
 public:
  SaverNone() = delete;
  SaverNone(wt2::WtParticleAlg* alg) : wt2::Saver(alg) { m_name = "None"; }
  virtual ~SaverNone() = default;

  SaverNone& operator=(SaverNone&&) = delete;
  SaverNone& operator=(const SaverNone&) = delete;
  SaverNone(SaverNone&&) = delete;
  SaverNone(const SaverNone&) = delete;

  TL::StatusCode defineOutputVariables(TTree* tree) override;
  TL::StatusCode assignOutputVariables() override;
};
}  // namespace wt2

#endif
