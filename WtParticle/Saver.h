#ifndef WTPARTICLE_SAVER_H
#define WTPARTICLE_SAVER_H

#include <TTree.h>
#include <TopLoop/Core/Utils.h>
#include <WtParticle/Config.h>
#include <regex>
#include <string>

class TTree;

namespace wt2 {
class WtParticleAlg;
}

namespace wt2 {
class Saver {
 protected:
  wt2::WtParticleAlg* m_alg;
  std::string m_name{""};

 public:
  Saver() = delete;
  Saver(wt2::WtParticleAlg* alg) : m_alg(alg) {}
  virtual ~Saver() = default;

  Saver& operator=(Saver&&) = delete;
  Saver& operator=(const Saver&) = delete;
  Saver(Saver&&) = delete;
  Saver(const Saver&) = delete;

  const std::string& name() const { return m_name; }

  virtual TL::StatusCode defineOutputVariables(TTree*) = 0;
  virtual TL::StatusCode assignOutputVariables() = 0;

 protected:
  template <class T>
  void make_branch(TTree* tree, const wt2::Config* c, const char* name, T* v) {
    for (auto const& exclude : c->excludes()) {
      std::regex r{exclude};
      if (std::regex_search(name, r)) return;
    }
    tree->Branch(name, v);
  }
};
}  // namespace wt2

#endif
