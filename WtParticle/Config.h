#ifndef WtParticle_Config_h
#define WtParticle_Config_h

#include <TopLoop/Core/FileManager.h>
#include <TopLoop/Core/Loggable.h>
#include <TopLoop/Core/SampleMetaSvc.h>
#include <TopLoop/EDM/Jet.h>

#include <set>
#include <vector>

namespace wt2 {
class Config : public TL::Loggable {
 public:
  Config() = default;
  Config(const std::string& filename);
  virtual ~Config() = default;

  Config(const Config&) = delete;
  Config(Config&&) = delete;
  Config& operator=(const Config&) = delete;
  Config& operator=(Config&&) = delete;

 private:
  // WtLoop configuration options
  std::vector<TL::FileManager::SubsetInstructions> m_subsetInstructions{};
  std::vector<std::string> m_disabled_branches{};
  bool m_save_MCfakes{false};
  bool m_allRegions{false};
  unsigned int m_max_njets{4};
  bool m_do_btagSFs{false};
  bool m_do_elmu{false};
  bool m_do_elel{false};
  bool m_do_mumu{false};
  bool m_do_OS{false};
  bool m_do_SS{false};
  float m_jet_pt_min{25.0};
 // wchar_t  m_loop_type{"partOnly"};
  float m_loose_jet_pt_min{20.0};
  float m_minimax_divider{0.0};
  bool m_manual_el_eta_veto{false};
  float m_scaled_lumi{1000.0};
  TL::EDM::BTagWP m_btagWP{TL::EDM::BTagWP::mv2c10_77};
  TL::EDM::BTagBin m_btagBin{TL::EDM::BTagBin::eff_77_70};
  std::vector<TL::kCampaign> m_campaigns{};

   bool m_save_with_flag{false};


  float m_OF_mll_divider{80};
  float m_OF_under_divider_met_min{50};
  float m_OF_over_divider_met_min{20};

  float m_SF_reject_Z_range{10};
  float m_SF_met_min{40};
  float m_SF_mll_min{40};
  float m_SF_mll_divider{100};
  float m_SF_under_divider_met_fac{4};
  float m_SF_under_divider_mll_fac{5};
  float m_SF_over_divider_met_fac{1};
  float m_SF_over_divider_mll_fac{2};
  float m_SF_over_divider_sum_min{300};

  std::vector<std::string> m_excludes{};
  std::vector<std::string> m_weight_excludes{};

  bool m_systematics_generator_variations{false};
  std::set<std::string> m_systematics_generator_variations_list{};

 public:
  const std::vector<TL::FileManager::SubsetInstructions>& subsetInstructions() const {
    return m_subsetInstructions;
  }
  const std::vector<std::string>& disabled_branches() const { return m_disabled_branches; }
  bool save_MCfakes() const { return m_save_MCfakes; }
  bool allRegions() const { return m_allRegions; }
  unsigned int max_njets() const { return m_max_njets; }
  bool do_btagSFs() const { return m_do_btagSFs; }
  bool do_elmu() const { return m_do_elmu; }
  bool do_elel() const { return m_do_elel; }
  bool do_mumu() const { return m_do_mumu; }
  bool do_OS() const { return m_do_OS; }
  bool do_SS() const { return m_do_SS; }
  float jet_pt_min() const { return m_jet_pt_min; }
  float loose_jet_pt_min() const { return m_loose_jet_pt_min; }
 // const loop_type() wchar_t { return m_loop_type; }

  float minimax_divider() const { return m_minimax_divider; }

  bool manual_el_eta_veto() const { return m_manual_el_eta_veto; }

 TL::EDM::BTagWP btagWP() const { return m_btagWP; }
  TL::EDM::BTagBin btagBin() const { return m_btagBin; }

  float scaled_lumi() const { return m_scaled_lumi; }

  const std::vector<TL::kCampaign>& campaigns() const { return m_campaigns; }

  bool save_with_flag() const { return m_save_with_flag; }

  float OF_mll_divider() const { return m_OF_mll_divider; }
  float OF_under_divider_met_min() const { return m_OF_under_divider_met_min; }
  float OF_over_divider_met_min() const { return m_OF_over_divider_met_min; }

  float SF_reject_Z_range() const { return m_SF_reject_Z_range; }
  float SF_met_min() const { return m_SF_met_min; }
  float SF_mll_min() const { return m_SF_mll_min; }
  float SF_mll_divider() const { return m_SF_mll_divider; }
  float SF_under_divider_met_fac() const { return m_SF_under_divider_met_fac; }
  float SF_under_divider_mll_fac() const { return m_SF_under_divider_mll_fac; }
  float SF_over_divider_met_fac() const { return m_SF_over_divider_met_fac; }
  float SF_over_divider_mll_fac() const { return m_SF_over_divider_mll_fac; }
  float SF_over_divider_sum_min() const { return m_SF_over_divider_sum_min; }

  const std::vector<std::string>& excludes() const { return m_excludes; }
  const std::vector<std::string>& weight_excludes() const { return m_weight_excludes; }

  bool systematics_generator_variations() const {
    return m_systematics_generator_variations;
  }

  const std::set<std::string>& systematics_generator_variations_list() const {
    return m_systematics_generator_variations_list;
  }

 public:
  void dump() const;
};
}  // namespace wt2

#endif
