/** @file  WtParticleAlgVars.h
 *  @brief WtParticleAlgVars class header
 *  @class WtParticleAlgVars
 *  @brief Variables container for hte WtParticleAlg algorithm
 *
 *  A clean place to hold all variables
 *
 *  @author Douglas Davis, <ddavis@cern.ch>
 */

#ifndef WtParticle_WtParticleAlgVars_h
#define WtParticle_WtParticleAlgVars_h

 #define BTAG_EIGENVAR_B_N 9
#define BTAG_EIGENVAR_C_N 3
#define BTAG_EIGENVAR_L_N 4
#define BTAG_CONT_EIGENVAR_B_N 45
#define BTAG_CONT_EIGENVAR_C_N 15
#define BTAG_CONT_EIGENVAR_L_N 20


#include <TopLoop/Core/Variables.h>

namespace wt2 {
  class WtParticleAlgVars {

  protected:

    /// meta variables

   TString m_metaYAML{};
    int   m_dsid{0};
    float m_totalNomWeights{0};

    float m_lumiWeight;
  
  UInt_t m_runNumber;
  UInt_t m_randomRunNumber;
  ULong64_t m_eventNumber;

  bool m_OS;
  bool m_SS;
  bool m_elmu;
  bool m_elel;
  bool m_mumu;
  bool m_1j1b;
  bool m_2j1b;
  bool m_2j2b;
  bool m_1j0b;
  bool m_2j0b;
  bool m_3j;
  bool m_4j;
  bool m_3j1b;
  bool m_3j2b;
  bool m_4j1b;
  bool m_2jXb;
  bool m_3pj;
  bool m_4j2b;
  bool m_4pj2pb;
  bool m_2j2bLmm;
  bool m_2j2bHmm;
  bool m_passed_metmll_cuts;
 // double  m_loop_type;
   float m_pT_lep1;
  float m_pT_lep2;
  float m_pT_jet1;
  float m_pT_jet2;
  float m_pT_jetL1;
  float m_pT_jetF;

  float m_eta_lep1;
  float m_eta_lep2;
  float m_eta_jet1;
  float m_eta_jet2;
  float m_eta_jetL1;
  float m_eta_jetF;

  float m_phi_lep1;
  float m_phi_lep2;
  float m_phi_jet1;
  float m_phi_jet2;
  float m_phi_jetL1;
  float m_phi_jetF;

  float m_E_lep1;
  float m_E_lep2;
  float m_E_jet1;
  float m_E_jet2;
  float m_E_jetL1;
  float m_E_jetF;

  float m_mass_jet1;
  float m_mass_jet2;
  float m_mass_jetL1;
  float m_mass_jetF;

  float m_met;
  float m_eta_met;
  float m_phi_met;
  float m_sumet;

  unsigned int m_njets;
  unsigned int m_nbjets;
  unsigned int m_nloosejets;
  unsigned int m_nloosebjets;
  float m_minimaxmbl;

  unsigned int m_pdgId_lep1;
  unsigned int m_pdgId_lep2;
  int m_charge_lep1;
  int m_charge_lep2;

  float m_mv2c10_jet1;
  float m_mv2c10_jet2;
  float m_mv2c10_jetF;
  float m_DL1_jetF;

  unsigned int m_psuedoContTagBin_jet1;
  unsigned int m_psuedoContTagBin_jet2;
  unsigned int m_psuedoContTagBin_jetF;

  int m_truthflav_jet1;
  int m_truthflav_jet2;
  int m_truthPartonLabel_jet1;
  int m_truthPartonLabel_jet2;
  bool m_isTrueHS_jet1;
  bool m_isTrueHS_jet2;

  float m_mT_jet1met;
  float m_mT_lep1met;
  float m_mT_lep2met;

  float m_eta_jetAvg;
  float m_deltaR_lep1lep2jetC_jetF;
 
 
  protected:

    // weight variables
    float m_weight_nominal;
 float m_weight_nominal_raw;
std::map<std::string, float> m_weight_sys_genvar_list_branches;
float m_weight_sys_radLo;
  float m_weight_sys_radHi;
float m_weight_sys_jvt_UP;
  float m_weight_sys_jvt_DOWN;
float m_weight_sys_pileup_UP;
  float m_weight_sys_pileup_DOWN;

 
 float m_weight_sys_leptonSF_EL_SF_Trigger_UP;
  float m_weight_sys_leptonSF_EL_SF_Trigger_DOWN;
  float m_weight_sys_leptonSF_EL_SF_Reco_UP;
  float m_weight_sys_leptonSF_EL_SF_Reco_DOWN;
  float m_weight_sys_leptonSF_EL_SF_ID_UP;
  float m_weight_sys_leptonSF_EL_SF_ID_DOWN;
  float m_weight_sys_leptonSF_EL_SF_Isol_UP;
  float m_weight_sys_leptonSF_EL_SF_Isol_DOWN;
  float m_weight_sys_leptonSF_MU_SF_Trigger_STAT_UP;
  float m_weight_sys_leptonSF_MU_SF_Trigger_STAT_DOWN;
  float m_weight_sys_leptonSF_MU_SF_Trigger_SYST_UP;
  float m_weight_sys_leptonSF_MU_SF_Trigger_SYST_DOWN;
  float m_weight_sys_leptonSF_MU_SF_ID_STAT_UP;
  float m_weight_sys_leptonSF_MU_SF_ID_STAT_DOWN;
  float m_weight_sys_leptonSF_MU_SF_ID_SYST_UP;
  float m_weight_sys_leptonSF_MU_SF_ID_SYST_DOWN;
  float m_weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
  float m_weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
  float m_weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
  float m_weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
  float m_weight_sys_leptonSF_MU_SF_Isol_STAT_UP;
  float m_weight_sys_leptonSF_MU_SF_Isol_STAT_DOWN;
  float m_weight_sys_leptonSF_MU_SF_Isol_SYST_UP;
  float m_weight_sys_leptonSF_MU_SF_Isol_SYST_DOWN;
  float m_weight_sys_leptonSF_MU_SF_TTVA_STAT_UP;
  float m_weight_sys_leptonSF_MU_SF_TTVA_STAT_DOWN;
  float m_weight_sys_leptonSF_MU_SF_TTVA_SYST_UP;
  float m_weight_sys_leptonSF_MU_SF_TTVA_SYST_DOWN;

  std::array<float, BTAG_EIGENVAR_B_N> m_weight_sys_bTagSF_MV2c10_77_eigenvars_B_up{};
  std::array<float, BTAG_EIGENVAR_B_N> m_weight_sys_bTagSF_MV2c10_77_eigenvars_B_down{};
  std::array<float, BTAG_EIGENVAR_C_N> m_weight_sys_bTagSF_MV2c10_77_eigenvars_C_up{};
  std::array<float, BTAG_EIGENVAR_C_N> m_weight_sys_bTagSF_MV2c10_77_eigenvars_C_down{};
  std::array<float, BTAG_EIGENVAR_L_N> m_weight_sys_bTagSF_MV2c10_77_eigenvars_Light_up{};
  std::array<float, BTAG_EIGENVAR_L_N> m_weight_sys_bTagSF_MV2c10_77_eigenvars_Light_down{};
  float m_weight_sys_bTagSF_MV2c10_77_extrapolation_up;
  float m_weight_sys_bTagSF_MV2c10_77_extrapolation_down;
  float m_weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_up;
  float m_weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_down;

  std::array<float, BTAG_EIGENVAR_B_N>
      m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_B_up{};
  std::array<float, BTAG_EIGENVAR_B_N>
      m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_B_down{};
  std::array<float, BTAG_EIGENVAR_C_N>
      m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_C_up{};
  std::array<float, BTAG_EIGENVAR_C_N>
      m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_C_down{};
  std::array<float, BTAG_EIGENVAR_L_N>
      m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_Light_up{};
  std::array<float, BTAG_EIGENVAR_L_N>
      m_weight_sys_bTagSF_MV2c10_Continuous_eigenvars_Light_down{};

 

  public:
    WtParticleAlgVars() = default;
    virtual ~WtParticleAlgVars() = default;

    WtParticleAlgVars(const WtParticleAlgVars&) = delete;
    WtParticleAlgVars(WtParticleAlgVars&&) = delete;
    WtParticleAlgVars& operator=(const WtParticleAlgVars&) = delete;
    WtParticleAlgVars& operator=(WtParticleAlgVars&&) = delete;

  };
}

#endif
